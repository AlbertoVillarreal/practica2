module Demultiplexer3to1
#(
	parameter NBits=16,
	parameter SELECTOR_BITS = 2 
)
(
	//Inputs
	input [SELECTOR_BITS - 1 : 0]Selector,
	input [NBits - 1 : 0]DEMUX_Input,
	
	//Outputs
	output [NBits - 1 : 0]DEMUX_Data0,
	output [NBits - 1 : 0]DEMUX_Data1,
	output [NBits - 1 : 0]DEMUX_Data2
	
);

enum logic[1:0]{DATA_0_SELECTED,DATA_1_SELECTED,DATA_2_SELECTED} cases;
logic [NBits-1:0]DEMUX_Data0_log;
logic [NBits-1:0]DEMUX_Data1_log;
logic [NBits-1:0]DEMUX_Data2_log;

	always_comb
	begin
	DEMUX_Data0_log = 0;
	DEMUX_Data1_log = 0;
	DEMUX_Data2_log = 0;
	
		case(Selector)
		DATA_0_SELECTED: DEMUX_Data0_log = DEMUX_Input;//00
		DATA_1_SELECTED: DEMUX_Data1_log = DEMUX_Input;//01
		DATA_2_SELECTED: DEMUX_Data2_log = DEMUX_Input;//10
		default:
		begin
			DEMUX_Data0_log = 0;
			DEMUX_Data1_log = 0;
			DEMUX_Data2_log = 0;
		end
		endcase
	end
	
	assign DEMUX_Data0 = DEMUX_Data0_log;
	assign DEMUX_Data1 = DEMUX_Data1_log;
	assign DEMUX_Data2 = DEMUX_Data2_log;
endmodule