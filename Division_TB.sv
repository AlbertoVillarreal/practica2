timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module Division_TB;
	bit clk = 0;
	bit reset;
	bit start;
	logic [15:0]dividend = -20;
	logic [15:0]divisor = -5;
	bit ready;
	logic [15:0]quotient;
	logic [15:0]remainder;

	
test_division
DUT
(
	.clk(clk),
	.reset(reset),
	.start(start),
	.dividend(dividend),
	.divisor(divisor),
	
	.quotient(quotient),
	.remainder(remainder)
);

initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  
  /*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#4	reset = 1;
	#4 start = 1;
end

endmodule