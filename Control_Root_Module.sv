module Control_Root_Module
(
	//Inputs
	input clk,
	input reset,
	input start,
	input finish_counting,
	input error, 	
	
	//Outputs
	output bit enable_Dregister,
	output bit enable_Rregister,
	output bit start_counting,
	output bit enable_Qregister,
	output bit for_cycle,
	output bit ready,
	output bit flush
	
);

	enum logic[3:0] {IDLE, LOAD, BEGIN, ACTIVATE_Q, LAST_OP, EXTRA, CALCULATE_R, READY, FLUSH} state, next_state;
	
	bit ready_bit;
	bit flush_bit;
	bit for_cycle_bit;
	bit start_counting_bit;
	bit enable_Dregister_bit;
	bit enable_Qregister_bit;
	bit enable_Rregister_bit;

	
	//state assignation, secuential process
	always_ff@(posedge clk or negedge reset)
	begin
		if(reset == 1'b0)
			state <= IDLE;
		else
			state <= next_state;
	end
	
	//state assignaation, combinational process
	always_comb
	begin
		
		case(state)
			IDLE:
			begin
				if(start == 1'b1)
					next_state = LOAD;
				else
					next_state = IDLE;
			end
			LOAD:
			begin
				if(error == 1)
					next_state = READY;
				else
					next_state = BEGIN;
			end
			BEGIN:
			begin
				next_state = ACTIVATE_Q;
			end
			ACTIVATE_Q:
			begin
				if(finish_counting == 1'b1)
					next_state = LAST_OP;
				else
					next_state = ACTIVATE_Q;
			end	
			LAST_OP:
			begin
				next_state = EXTRA;
			end	
			EXTRA:
			begin
				next_state = CALCULATE_R;
			end
			CALCULATE_R:
			begin
				next_state = READY;
			end
			READY:
			begin
				if(start == 1'b0)
					next_state = FLUSH;
				else
					next_state = READY;
			end	
			FLUSH:
			begin
				next_state = IDLE;
			end
			default:
			begin
				next_state = IDLE;
			end
		endcase
	end
	
	//Outputs assignations, combinational process
	always_comb
	begin
		ready_bit = 1'b0;
		flush_bit = 1'b0;
		for_cycle_bit = 1'b0;
		start_counting_bit = 1'b0;
		enable_Dregister_bit = 1'b0;
		enable_Qregister_bit = 1'b0;
		enable_Rregister_bit = 1'b0;

		case(state)
			IDLE:
			begin
			end
			LOAD:
			begin
				enable_Dregister_bit = 1'b1;
				enable_Rregister_bit = 1'b1;
				start_counting_bit = 1'b1;
				for_cycle_bit = 1'b1;
			end	
			BEGIN:
			begin
				enable_Rregister_bit = 1'b1;
				for_cycle_bit = 1'b1;
				start_counting_bit = 1'b1;
				enable_Qregister_bit = 1'b1;
			end
			ACTIVATE_Q:
			begin
				enable_Rregister_bit = 1'b1;
				enable_Qregister_bit = 1'b1;
				start_counting_bit = 1'b1;
				for_cycle_bit = 1'b1;
			end		
			LAST_OP:
			begin
				enable_Rregister_bit = 1'b1;
				enable_Qregister_bit = 1'b1;
				for_cycle_bit = 1'b1;
			end
			EXTRA:
			begin
				//enable_Rregister_bit = 1'b1;
				//enable_Qregister_bit = 1'b1;//
				for_cycle_bit = 1'b1;//
			end
			CALCULATE_R:
			begin
				enable_Rregister_bit = 1'b1;//
				//enable_Qregister_bit = 1'b1;//
				//for_cycle_bit = 1'b1;//
			end
			READY:
			begin
				ready_bit = 1'b1;
			end	
			FLUSH:
			begin
				flush_bit = 1'b1;
			end	
			default:
			begin
				ready_bit = 1'b0;
				flush_bit = 1'b0;
				for_cycle_bit = 1'b0;
				start_counting_bit = 1'b0;
				enable_Dregister_bit = 1'b0;
				enable_Qregister_bit = 1'b0;
				enable_Rregister_bit = 1'b0;

			end
		endcase		
	end
	
	assign enable_Dregister = enable_Dregister_bit;
	assign enable_Rregister = enable_Rregister_bit;
	assign enable_Qregister = enable_Qregister_bit;
	assign start_counting = start_counting_bit;
	assign for_cycle = for_cycle_bit;
	assign ready = ready_bit;
	assign flush = flush_bit;
	
endmodule