module OR_Module
#(
	parameter NBits = 16,
	parameter Negativo = 1
)
(
	//Input
	input bit sign,
	input [NBits - 1 : 0]data_in_0,
	input [NBits - 1 : 0]data_in_1,
	
	//Output
	output [NBits - 1 : 0]data_out
);

logic [NBits - 1 : 0]data_out_log;

always_comb
begin

	if(sign == Negativo)
		data_out_log = (data_in_0 | data_in_1);
	else
		data_out_log = -(data_in_0 | data_in_1);
	
end

assign data_out = data_out_log;

endmodule