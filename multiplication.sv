module multiplication
#(
	parameter DATA_LENGTH_16 = 16,
	parameter DATA_LENGTH_32 =32
)
(
	input clk,
	input reset,
	input start,
	input [DATA_LENGTH_16 - 1 :0]multiplier,
	input [DATA_LENGTH_16 - 1 :0]multiplicand,
	
	output [DATA_LENGTH_32 - 1 :0]product
);

logic [DATA_LENGTH_16 - 1 :0]adder_output_wire;
logic [DATA_LENGTH_16 - 1 :0]first_adder_output_wire;
logic [DATA_LENGTH_16 - 1 :0]second_adder_output_wire;

bit ready_flag_wire;

Multi_Booth
#(
	.DATALENGTH(16),
	.DATALENGTH_PRODUCT(32)
)
MULTIPLICATION
(
	.multiplier(multiplier),
	.multiplicand(multiplicand),
	.adder(adder_output_wire),
	.start(start),
	.clk(clk),
	.reset(reset),
	//Outputs
	.product(product),
	.ready(ready_flag_wire),
	.first_adder_output(first_adder_output_wire),
	.second_adder_output(second_adder_output_wire)
);

Adder
#(
	.variable_size(16)
)
ADDER_M
(
	//Inputs
	.variable_sum1(first_adder_output_wire),
	.variable_sum2(second_adder_output_wire),
	.SOR(1),
	//Outputs
	.sum_output(adder_output_wire)
);
endmodule