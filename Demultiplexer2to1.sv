module Demultiplexer2to1
#(
	parameter NBits=16
)
(
	//Inputs
	input Selector,
	input [NBits - 1 : 0]DEMUX_Input,
	
	//Outputs
	output [NBits - 1 : 0]DEMUX_Data0,
	output [NBits - 1 : 0]DEMUX_Data1
	
);

enum logic[1:0]{DATA_0_SELECTED,DATA_1_SELECTED} cases;
logic [NBits-1:0]DEMUX_Data0_log;
logic [NBits-1:0]DEMUX_Data1_log;

	always_comb
	begin
	DEMUX_Data0_log = 0;
	DEMUX_Data1_log = 0;
	
		case(Selector)
		DATA_0_SELECTED: DEMUX_Data0_log = DEMUX_Input;//0
		DATA_1_SELECTED: DEMUX_Data1_log = DEMUX_Input;//1
		default:
		begin
			DEMUX_Data0_log = 0;
			DEMUX_Data1_log = 0;
		end
		endcase
	end
	
	assign DEMUX_Data0 = DEMUX_Data0_log;
	assign DEMUX_Data1 = DEMUX_Data1_log;
endmodule