module Root_Module
#(
	parameter Nbits = 16
)
(
	input clk,
	input reset,
	input start,
	input [Nbits - 1 : 0]DataX,
	
	
	output ready,
	output [Nbits - 1 : 0]raiz,
	output [Nbits - 1 : 0]residuo,
	output [Nbits - 1 : 0]root_final_result,
	output [Nbits - 1 : 0]remainder_final_result
	
);

	bit start_counting_bit;
	bit finish_counting_bit;
	logic [Nbits - 1 : 0]Adder_to_raizModule;
	logic [Nbits - 13 : 0]counter_log;

Raiz
#(
	.Nbits(Nbits)
)
MODULO_RAIZ
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.finish_counting(finish_counting_bit),
	.counter(counter_log),
	.data_in_Adder(Adder_to_raizModule),
	.data_in_X(DataX),
	
	//Outputs
	.ready(ready),
	.start_counting(start_counting_bit),
	.remainder(residuo),
	.result(raiz),
	.root_final_result(root_final_result),
	.remainder_final_result(remainder_final_result)
);

Adder
#(
	.variable_size(Nbits)
)
ADDER
(
	//Inputs
	.variable_sum1(residuo),
	.variable_sum2(raiz),
	
	//Outputs
	.sum_output(Adder_to_raizModule)
);

CounterWithFunction
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(Nbits),
	.NBITS_FOR_COUNTER(CeilLog2(Nbits))
)
COUNTER

(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(start_counting_bit),
	
	// Output Ports
	.flag(finish_counting_bit),
	.CountOut(counter_log) 
);



//----------------------------------------------------------------------------------------------

/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

endmodule