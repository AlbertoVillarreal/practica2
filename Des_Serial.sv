module Des_Serial
#(
	parameter Nbits = 16
)
(
	//Inputs
	input clk,
	input reset,
	input loadX,
	input loadY,
	input [Nbits - 1 : 0]Data_In,
	
	//Outputs
	output [Nbits - 1 : 0]DataX_Out,
	output [Nbits - 1 : 0]DataY_Out
);

logic [Nbits - 1 : 0]DEMUX_Data_to_X_Register;
logic [Nbits - 1 : 0]DEMUX_Data_to_Y_Register;

/************************************************************************************************
	Demultiplexor 2 a 1 dato 1. 
	Se elige entre 2 salidas. Tomamos el dato de entrada y lo asignamos al registro X o Y dependiendo
	de la bandera de load.
	Entradas:
		[Selector]: loadY.
		[MUX_Input]: Dato de entrada al sistema, Data_In
	
	Salidas:

		[DEMUX_Data0]: Dato a registro X
		[DEMUX_Data1]: Dato a registro Y
		
*/	
Demultiplexer2to1
#(
	.NBits(Nbits)
)
DEMUX_DATA_XY
(
	//Inputs
	.Selector(loadY),
	.DEMUX_Input(Data_In),
	
	//Outputs
	.DEMUX_Data0(DEMUX_Data_to_X_Register),
	.DEMUX_Data1(DEMUX_Data_to_Y_Register)
	
);


/************************************************************************************************
	Registro de dato X. 
	Guardamos dato de entrada X.
	Entradas:
		[enable]: loadX
		[Data_Input]: DEMUX_Data0 de demultiplexor
	
	Salidas:
		[Data_Output]: DataX_Out
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
X_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(loadX),
	.sync_reset(0),
	.Data_Input(DEMUX_Data_to_X_Register),

	// Output Ports
	.Data_Output(DataX_Out)
);

/************************************************************************************************
	Registro de dato Y. 
	Guardamos dato de entrada Y.
	Entradas:
		[enable]: loadY
		[Data_Input]: DEMUX_Data1 de demultiplexor
	
	Salidas:
		[Data_Output]: DataY_Out
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
Y_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(loadY),
	.sync_reset(0),
	.Data_Input(DEMUX_Data_to_Y_Register),

	// Output Ports
	.Data_Output(DataY_Out)
);
endmodule