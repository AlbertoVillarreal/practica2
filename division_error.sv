module division_error
#(
	parameter DATA_LENGTH = 16	
)
(
	input [DATA_LENGTH - 1 : 0]dividend,
	input [DATA_LENGTH - 1 : 0]divisor,
	
	output error,
	output [DATA_LENGTH - 1 : 0]remainder,
	output [DATA_LENGTH - 1 : 0]quotient
);
bit error_wire;
logic [DATA_LENGTH - 1 : 0]remainder_wire;
logic [DATA_LENGTH - 1 : 0]quotient_wire;
always_comb
begin
	if(divisor == 0)
	begin
		error_wire = 1;
		remainder_wire = 0;
		quotient_wire = 0;
	end
	else if (divisor > dividend)
	begin
		quotient_wire = 0;
		remainder_wire = divisor;
		error_wire = 1;
	end
	else
	begin
	error_wire = 0;
	remainder_wire = 0;
	quotient_wire = 0;
	end
end

assign remainder = remainder_wire;
assign quotient = quotient_wire;
assign error = error_wire;
endmodule