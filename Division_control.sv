/********************************************************************
* Name:
*	Concatenation.sv
*
* Description:
* 	This module receives two inputs, concatenates it and send it out
*
* Inputs:
*	clk: 							Board clock .
*	reset:						Asynchronous reset.
*	AOS:							Bit to see if we are doing an addition or a substraction
*  start:						Bit wich indicates te beggining of the state machine and the algorithm
*	algorithm_finished:		Bit wich indicates the end of the algorithm
* Outputs:
* 	initial_flag: 				Flag wich indicates the data to use is the initial one.
*	continuous_flag:			Flag wich indicates the data to use is the continuous one.
*	ready_flag:					Flag wich indicates the data is ready to send out.
*	flush_flag:					Flag wich indicates the flush of the register.
*	count_init:					Flag wich indicates the initialization of the counter.
*	SOR:							Flag wich indicates the add or the substraction.
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	12/3/2018 
*
*********************************************************************/
module Division_control
(
	//Inputs
	input clk,
	input reset,
	input start,
	input bit AOS,
	input bit algorithm_finished,
	input [4:0]counter,
	input error,
	//Outputs
	output bit initial_flag,
	output bit continuous_flag,
	output bit ready_flag,
	output bit flush_flag,
	output bit count_init,
	output bit final_condition_flag,
	output bit SOR
);
enum logic[2:0] {IDLE, BEGIN, ALGORITHM, READY, FLUSH} state, next_state;
bit initial_flag_wire;
bit continuous_flag_wire;
bit ready_flag_wire;
bit flush_flag_wire;
bit count_init_flag_wire;
bit SOR_flag_wire;
bit final_condition_flag_wire;
always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		state <= IDLE;
	else
		state <= next_state;
end

always_comb
begin
	case(state)
		IDLE:
		begin
			if(start)
				next_state = BEGIN;
			else
				next_state = IDLE;
		end
		BEGIN:
		begin
			if(error)
				next_state = READY;
			else
				next_state = ALGORITHM;
		end
		ALGORITHM:
		begin
			if(algorithm_finished)
				next_state = READY;
			else
				next_state = ALGORITHM;
		end
		READY:
			if(start == 0)
				next_state = FLUSH;
			else
				next_state = READY;		
		FLUSH:
			next_state = IDLE;
		default:
			next_state = IDLE;
		endcase
end
//Outputs assignations, combinational process
always_comb
begin
	initial_flag_wire = 0;
	continuous_flag_wire = 0;
	ready_flag_wire = 0;
	flush_flag_wire = 0;
	count_init_flag_wire = 0;
	final_condition_flag_wire = 0;
	SOR_flag_wire = 0;
	case(state)
		IDLE:
		begin
		end
		BEGIN:
		begin
			count_init_flag_wire = 1;
			initial_flag_wire = 1;
		end
		ALGORITHM:
		begin
			count_init_flag_wire = 1;
			continuous_flag_wire = 1;
			if(AOS)
				SOR_flag_wire = 1;
			else
				SOR_flag_wire = 0;
			if(counter > 16)
				final_condition_flag_wire = 1;
			else
				final_condition_flag_wire = 0;
		end
		READY:
		begin
			if(AOS)
				SOR_flag_wire = 1;
			else
				SOR_flag_wire = 0;
			ready_flag_wire = 1;
			final_condition_flag_wire = 1;
		end
		FLUSH:
		begin
			flush_flag_wire = 1;
		end
		default:
		begin
			initial_flag_wire = 0;
			continuous_flag_wire = 0;
			ready_flag_wire = 0;
			flush_flag_wire = 0;
			count_init_flag_wire = 0;
			final_condition_flag_wire = 0;
			SOR_flag_wire = 0;
		end
	endcase
end

	assign initial_flag = initial_flag_wire;
	assign continuous_flag = continuous_flag_wire;
	assign ready_flag = ready_flag_wire;
	assign flush_flag = flush_flag_wire;
	assign count_init = count_init_flag_wire;
	assign final_condition_flag = final_condition_flag_wire;
	assign SOR = SOR_flag_wire;
endmodule