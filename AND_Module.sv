module AND_Module
#(
	parameter Nbits = 16
)
(
	//Inputs
	input [Nbits - 1 : 0]data_in_0,
	input [Nbits - 1 : 0]data_in_1,
	
	//Outputs
	output [Nbits - 1 : 0]data_out
);

logic [Nbits - 1 : 0]data_out_log;

always_comb
begin
	
	data_out_log = (data_in_0 & data_in_1);
	
end

assign data_out = data_out_log;

endmodule