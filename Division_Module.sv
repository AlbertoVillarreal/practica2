module Division_Module
#(
	parameter Nbits = 16
)
(
	//Inputs
	input clk,
	input reset,
	input start,
	input [Nbits - 1 : 0]dividend,
	input [Nbits - 1 : 0]divisor,
	input [Nbits - 1 : 0]adderFeedback,
	
	//Outputs
	output ready,
	output SOR,
	output error,
	output sign,
	output [Nbits - 1 : 0]first_adder_output,
	output [Nbits - 1 : 0]second_adder_output,
	output [Nbits - 1 : 0]quotient,
	output [Nbits - 1 : 0]remainder
);

	bit dividend_sign;
	bit divisor_sign;
	bit ready_wire;
	
	logic [Nbits - 1 : 0]quotient_wire;
	logic [Nbits - 1 : 0]remainder_wire;
	logic [Nbits - 1 : 0]dividend_VA;
	logic [Nbits - 1 : 0]divisor_VA;

	logic[31:0] multiplexor_wire;

/************************************************************************************************

*/
ValorAbsoluto
#(
	.word_length(Nbits)
 )
 VALOR_ABSOLUTO_DIVIDEND
(

	 //Input Ports
    .entrada(dividend),
	 
	 //Output Ports
    .salida(dividend_VA),
    .signo(dividend_sign)
 );

/************************************************************************************************

*/
 ValorAbsoluto
#(
	.word_length(Nbits)
 )
 VALOR_ABSOLUTO_DIVISOR
(

	 //Input Ports
    .entrada(divisor),
	 
	 //Output Ports
    .salida(divisor_VA),
    .signo(divisor_sign)
 );
 
/************************************************************************************************

*/
DivisionNonRestoring
#(
	.DATA_LENGTH_16(Nbits),
	.DATA_LENGTH_32((Nbits*2))
)
DIVISION
(
	//Inputs
	.dividend(dividend_VA),
	.divisor(divisor_VA),
	.adder_in(adderFeedback),
	.clk(clk),
	.reset(reset),
	.start(start),

	//Outputs
	.quotient(quotient_wire),
	.remainder(remainder_wire),
	.adder_out(first_adder_output),
	.ready(ready_wire),
	.error(error),
	.SOR(SOR)
);

/************************************************************************************************

*/
Multiplexer2to1
#(
	.NBits((Nbits*2))
)
MULTIPLEXOR_DIVISION
(
	.Selector(ready_wire),
	.MUX_Data0(0),
	.MUX_Data1({remainder_wire,quotient_wire}),
	
	.MUX_Output(multiplexor_wire)

);

assign sign = ((dividend_sign || divisor_sign) && ready_wire);
assign ready = ready_wire;
assign second_adder_output = divisor_VA;
assign quotient = multiplexor_wire[Nbits - 1 : 0];
assign remainder = multiplexor_wire[31 : Nbits];

endmodule