/********************************************************************
* Name:
*	Register_Data_Enable.sv
*
* Description:
* 	This module is a register and recive 2 data depending of the flag is tha value that is going to be used and send 3.
*
* Inputs:
*	clk: 									Board clock.
*	reset:								asynchronous reset.
*	enable_data1_flag:				enable for the first data.
*	enable_data2_flag:				enable for the second data.
*	Data_Input1:						first data, it's the multiplier.
*	Data_Input2:						second data it's the new data that is continuous changed by the algorithm.
*	Start:								Bit tha indicates the begining of the algorithm
* Outputs:
* 	Data_Output_adder: 				Adder data.
* 	Data_Output_concatenation: 	rigth value of the concatenation.
*	Data_Output_final:				all the data that is changed by the algortithm each clock cycle.
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	12/3/2018 
*
*********************************************************************/
module Register_Data_Enable
#(
	parameter DATA_LENGTH_16 = 16,
	parameter DATA_LENGTH_33 = 33
	
)
(
	// Input Ports
	input clk,
	input reset,
	input enable_data1_flag,
	input enable_data2_flag,
	input [DATA_LENGTH_33 - 1:0] Data_Input1,
	input [DATA_LENGTH_33 - 1:0] Data_Input2,
	input start,
	input flush,
	// Output Ports
	output [DATA_LENGTH_16 - 1:0] Data_Output_adder,
	output [DATA_LENGTH_16 : 0] Data_Output_concatenation,
	output [DATA_LENGTH_33 - 2 : 0] Data_Output_final
	
);
	logic [DATA_LENGTH_33 - 1 : 0] Data_input_wire;
	logic [DATA_LENGTH_16 - 1:0] Data_Output_adder_wire;
	logic [DATA_LENGTH_16 : 0] Data_Output_concatenation_wire;
	logic [DATA_LENGTH_33 - 2 : 0] Data_Output_final_wire;
	
	always_ff@(posedge clk or negedge reset)
	begin
		if(reset == 1'b0)
			Data_input_wire <= 0;
		else if(start)
		begin
			if(enable_data1_flag == 1'b1)//Bandera que marca el valor que debe de sacar
				Data_input_wire <=  Data_Input1;
			else
				Data_input_wire <= Data_Input2;
			if(flush)
				Data_input_wire <= 0;
				Data_Output_adder_wire <= 0;
				Data_Output_concatenation_wire <= 0;
				Data_Output_final_wire <= 0;
				
		Data_Output_adder_wire <= Data_input_wire[DATA_LENGTH_33 - 1 : DATA_LENGTH_16 + 1];
		Data_Output_concatenation_wire <= Data_input_wire[DATA_LENGTH_16 : 0];
		Data_Output_final_wire <= Data_input_wire[DATA_LENGTH_33 - 1 : 1];
		end
			
	end
assign Data_Output_adder = Data_Output_adder_wire;
assign Data_Output_concatenation = Data_Output_concatenation_wire;
assign Data_Output_final = Data_Output_final_wire;
	
endmodule