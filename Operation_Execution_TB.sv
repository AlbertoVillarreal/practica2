timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module Operation_Execution_TB;
		//Inputs
	logic clk = 0;
	logic reset = 0;
	logic start = 0;
	logic [16 - 15 : 0]Opcode;
	//input [Nbits - 1 : 0]DataFeedbackAdder,
	logic [16 - 1 : 0]DataX = 0;
	logic [16 - 1 : 0]DataY = 0;
	
	//Outputs
	logic ready_log;
	logic error_log;
	//output [Nbits - 1 : 0]data_adder_1,
	//output [Nbits - 1 : 0]data_adder_2,
	logic [16 - 1 : 0]result_log;
	logic [16 - 1 : 0]remainder_log;
	
Operation_Execution_Test	
DUT
(
		//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.Opcode(Opcode),
	//input [Nbits - 1 : 0]DataFeedbackAdder,
	.DataX(DataX),
	.DataY(DataY),
	
	//Outputs
	.ready(ready_log),
	.error(error_log),
	//output [Nbits - 1 : 0]data_adder_1,
	//output [Nbits - 1 : 0]data_adder_2,
	.result(result_log),
	.remainder(remainder_log)
	
);


initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  
  
  
  /*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#6 reset = 1;
	#4 Opcode = 0;
	#6 DataX = 200;
	#0 DataY = 3;
	#2 start = 1;
	//#160 start = 0;
	//#10 start = 1;
	
end

endmodule