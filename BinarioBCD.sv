/********************************************************************
* Name:
*	BinarioBCD.sv
*
* Description:
* 	This module receives data from in binary format, and
* 	converts it to BCD data using double dabble algorithm.
*
* Inputs:
*	data_input: 							Binary data from switches in binary format.
*
* Outputs:
* 	hundreds_output: 						Hundreds in BCD.
* 	tens_output: 							Tens in BCD.
* 	units_output: 							Units in BCD.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	3/2/2018 
*
*********************************************************************/
module BinarioBCD
(
	//Input ports
	input [15 : 0] data_input,
	
	//Output ports
	output [3 : 0] units_output,
	output [3 : 0] tens_output,
	output [3 : 0] hundreds_output,
	output [3 : 0] thousands_units_output,
	output [3 : 0] thousands_tens_output
);
logic [5 : 0] i_log;
logic [3 : 0] units_log;
logic [3 : 0] tens_log;
logic [3 : 0] hundreds_log;
logic [3 : 0] thousands_units_log;
logic [3 : 0] thousands_tens_log;
always_comb 

	begin: ThisIsADeco

		//Initialize vector values to make a new conversion.
		units_log = 4'd0;
		tens_log = 4'd0;
		hundreds_log = 4'd0;
		thousands_units_log = 4'd0;
		thousands_tens_log = 4'd0;
		//Double dabble algorithm
		for(i_log=16 ; i_log > 0; i_log=i_log - 1'd1)
			begin
				//If each one of the register takes a value of 5 add 3
				if(thousands_tens_log >= 5)
					thousands_tens_log = thousands_tens_log + 2'd3;
				if(thousands_units_log >= 5)
					thousands_units_log = thousands_units_log + 2'd3;
				if(hundreds_log >= 5)
					hundreds_log = hundreds_log + 2'd3;
				if(tens_log >= 5)
					tens_log = tens_log + 2'd3;
				if(units_log >= 5)
					units_log = units_log + 2'd3;
				
				//Shift the registers until we reach the last one bit (LSB) of the input
				thousands_tens_log = thousands_tens_log << 1;
				thousands_tens_log[0] = thousands_units_log[3]; 	
				thousands_units_log = thousands_units_log << 1;
				thousands_units_log[0] = hundreds_log[3];  
				hundreds_log = hundreds_log << 1;
				hundreds_log[0] = tens_log[3];
				tens_log = tens_log << 1;
				tens_log[0] = units_log[3];
				units_log = units_log << 1;
				units_log[0] = data_input[i_log-1];
			end

	end: ThisIsADeco

//Assign output values.
assign units_output = units_log;
assign tens_output = tens_log;
assign hundreds_output = hundreds_log;
assign thousands_units_output = thousands_units_log;
assign thousands_tens_output = thousands_tens_log;

endmodule 