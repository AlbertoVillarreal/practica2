/********************************************************************
* Name:
*	Concatenation.sv
*
* Description:
* 	This module receives two inputs, concatenates it and send it out
*
* Inputs:
*	data_1: 						One part of the data.
*	data_2:						Other part of the data.
* Outputs:
* 	data_concatenated: 		Data concatenated, the first data goes to the MSB and the second data LSB.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	12/3/2018 
*
*********************************************************************/
module Concatenation
#(
	parameter DATA_LENGTH = 16,
	parameter DATA_CONCATENATED_LENGTH = 33,
	parameter DATA_LENGTH_DATA2 = 17
)

(
	//Inputs
	input [DATA_LENGTH - 1 : 0]data_1,
	input [DATA_LENGTH_DATA2 - 1 : 0]data_2,
	//Outputs
	output [DATA_CONCATENATED_LENGTH - 1 : 0]data_concatenated
);

	logic [DATA_CONCATENATED_LENGTH - 1 : 0]data_concatenated_wire;
	
always_comb
begin
	data_concatenated_wire = {data_1,data_2};
end

assign data_concatenated = data_concatenated_wire;
endmodule