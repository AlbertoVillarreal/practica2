module test_division
(
	input clk,
	input reset,
	input start,
	input [15:0]dividend,
	input [15:0]divisor,
	
	output [15:0]quotient,
	output [15:0]remainder
);

logic[15:0] quotient_wire;
logic[15:0] remainder_wire;
logic[15:0] adder_out_wire;
logic[15:0] adder_outA_wire;
logic[15:0]	dividend_VA;
logic[15:0]	divisor_VA;
bit dividend_sign;
bit divisor_sign;
bit ready_wire;
bit error_wire;
logic[31:0] multiplexor_wire;
bit SOR_wire;

ValorAbsoluto
#(
	.word_length(16)
 )
 VALOR_ABSOLUTO_DIVIDEND
(

	 //Input Ports
    .entrada(dividend),
	 
	 //Output Ports
    .salida(dividend_VA),
    .signo(dividend_sign)
 );
 
 ValorAbsoluto
#(
	.word_length(16)
 )
 VALOR_ABSOLUTO_DIVISOR
(

	 //Input Ports
    .entrada(divisor),
	 
	 //Output Ports
    .salida(divisor_VA),
    .signo(divisor_sign)
 );
 
 
DivisionNonRestoring
#(
	.DATA_LENGTH_16(16),
	.DATA_LENGTH_32(32)
)
DIVISION
(
	//Inputs
	.dividend(dividend_VA),
	.divisor(divisor_VA),
	.adder_in(adder_outA_wire),
	.clk(clk),
	.reset(reset),
	.start(start),

	//Outputs
	.quotient(quotient_wire),
	.remainder(remainder_wire),
	.adder_out(adder_out_wire),
	.ready(ready_wire),
	.error(error_wire),
	.SOR(SOR_wire)
);
adder
#(
	.variable_size(16)
)
ADDER_DIVISION
(
	//Inputs
	.variable_sum1(adder_out_wire),
	.variable_sum2(divisor_VA),
	.SOR(SOR_wire),
	//Outputs
	.sum_output(adder_outA_wire)
);

Multiplexer2to1
#(
	.NBits(32)
)
MULTIPLEXOR_DIVISION
(
	.Selector(ready_wire),
	.MUX_Data0(0),
	.MUX_Data1({remainder_wire,quotient_wire}),
	
	.MUX_Output(multiplexor_wire)

);
assign quotient = multiplexor_wire[15:0];
assign remainder = multiplexor_wire[31:16];
endmodule
