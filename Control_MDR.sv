module Control_MDR
(
	//Inputs
	input clk,
	input reset,
	input start,				//boton de la tarjeta
	input load,					//boton de la tarjeta
	input ready,				//salida de layer 2
	input use_last_data,		//boton de la tarjeta
	
	//Outputs
	output loadX,				//Para registro de Des-Serial
	output loadXLED,			//Led de la tarjeta para indicar al usuario el ingreso de dato
	output loadY,				//Para registro de Des-Serial
	output loadYLED,			//Led de la tarjeta para indicar al usuario el ingreso de dato
	output start_op,			//Para layer 2, indica el inicio de la operacion
	output flush				//Para registros que se conectan al final de los 7 segmentos
	
);

	enum logic[3:0] {IDLE, WAIT_X, LOAD_X, WAIT_Y, LOAD_Y, OPERATION_EXECUTE, READY, FLUSH} state, next_state;
	
	logic loadX_bit;
	logic loadXLED_bit;
	logic loadY_bit;
	logic loadYLED_bit;
	logic start_op_bit;
	logic flush_bit;
	
		//state assignation, secuential process
	always_ff@(posedge clk or negedge reset)
	begin
		if(reset == 1'b0)
			state <= IDLE;
		else
			state <= next_state;
	end
	
	//state assignaation, combinational process
	always_comb
	begin
		
		case(state)
			IDLE:
			begin
				if(start == 1'b1)
					next_state = WAIT_X;
				else
					next_state = IDLE;
			end
			WAIT_X:
			begin
				if(load == 1'b0)
					next_state = LOAD_X;
				else if(use_last_data == 1'b0)
					next_state = OPERATION_EXECUTE;
				else
					next_state = WAIT_X;
			end
			LOAD_X:
			begin
				if(load == 1'b1)
					next_state = WAIT_Y;
				else
					next_state = LOAD_X;
			end
			WAIT_Y:
			begin
				if(load == 1'b0)
					next_state = LOAD_Y;
				else
					next_state = WAIT_Y;
			end
			LOAD_Y:
			begin
				if(load == 1'b1)
					next_state = OPERATION_EXECUTE;
				else
					next_state = LOAD_Y;
			end
			OPERATION_EXECUTE:
			begin
				if(ready == 1'b1)
					next_state = READY;
				else
					next_state = OPERATION_EXECUTE;
			end
			READY:
			begin
				if(start == 1'b0)
					next_state = FLUSH;
				else
					next_state = READY;
			end
			FLUSH:
			begin
				next_state = IDLE;
			end
			default:
			begin
				next_state = IDLE;
			end
			
		endcase
	end
	
	//Outputs assignations, combinational process
	always_comb
	begin
	 loadX_bit = 1'b0;
	 loadXLED_bit = 1'b0;
	 loadY_bit = 1'b0;
	 loadYLED_bit = 1'b0;
	 start_op_bit = 1'b0;
	 flush_bit = 1'b0;
	 
		case(state)
			IDLE:
			begin
				
			end
			WAIT_X:
			begin
				loadXLED_bit = 1'b1;
			end
			LOAD_X:
			begin
				loadX_bit = 1'b1;
			end
			WAIT_Y:
			begin
				loadYLED_bit = 1'b1;
			end
			LOAD_Y:
			begin
				loadY_bit = 1'b1;
			end
			OPERATION_EXECUTE:
			begin
				start_op_bit = 1'b1;
			end
			READY:
			begin
				start_op_bit = 1'b1;
			end
			FLUSH:
			begin
				flush_bit = 1'b1;
			end
			default:
			begin
				 loadX_bit = 1'b0;
				 loadXLED_bit = 1'b0;
				 loadY_bit = 1'b0;
				 loadYLED_bit = 1'b0;
				 start_op_bit = 1'b0;
				 flush_bit = 1'b0;
			end
		endcase
		
	end
	
	assign loadX = loadX_bit;
	assign loadXLED = loadXLED_bit;
	assign loadY = loadY_bit;
	assign loadYLED = loadYLED_bit;
	assign start_op = start_op_bit;
	assign flush = flush_bit;
	
endmodule