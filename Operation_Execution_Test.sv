module Operation_Execution_Test
#(
	parameter Nbits = 16
)
(
		//Inputs
	input clk,
	input reset,
	input start,
	input [Nbits - 15 : 0]Opcode,
	input [Nbits - 1 : 0]DataX,
	input [Nbits - 1 : 0]DataY,
	
	//Outputs
	output ready,
	output error,
	output [Nbits - 1 : 0]result,
	output [Nbits - 1 : 0]remainder
);


	logic [Nbits - 1 : 0]Data1_Adder;
	logic [Nbits - 1 : 0]Data2_Adder;
	logic [Nbits - 1 : 0]Data_Feedback;
	
Operation_Execution
#(
	.Nbits(Nbits)
)
LAYER_2
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.Opcode(Opcode),
	.DataFeedbackAdder(Data_Feedback),
	.DataX(DataX),
	.DataY(DataY),
	
	//Outputs
	.ready(ready),
	.error(error),
	.data_adder_1(Data1_Adder),
	.data_adder_2(Data2_Adder),
	.result(result),
	.remainder(remainder)
	
);

Adder
#(
	.variable_size(Nbits)
)
ADDER
(
	//Inputs
	.variable_sum1(Data1_Adder),
	.variable_sum2(Data2_Adder),
	
	//Outputs
	.sum_output(Data_Feedback)
);


endmodule