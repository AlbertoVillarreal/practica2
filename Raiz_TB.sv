timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module Raiz_TB;
	//Inputs
	logic clk = 0;
	logic reset;
	logic start = 0;
	logic [16 - 1 : 0]DataX = 0;
	
	//Outputs
	logic ready_log;
	logic [16 - 1 : 0]raiz_log;
	logic [16 - 1 : 0]residuo_log;
	logic [16 - 1 : 0]root_final_result_log;
	logic [16 - 1 : 0]remainder_final_result_log;
	
Root_Module	
DUT
(
	.clk(clk),
	.reset(reset),
	.start(start),
	.DataX(DataX),
	
	.ready(ready_log),
	.raiz(raiz_log),
	.residuo(residuo_log),
	.root_final_result(root_final_result_log),
	.remainder_final_result(remainder_final_result_log)
	
);


initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  
  
  
  /*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#6 reset = 1;
	#6 DataX = 65535;
	#2 start = 1;
	
end

endmodule