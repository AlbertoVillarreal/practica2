module Multiplication_control
#(
	parameter DATA_LENGTH = 2,
	parameter MAX_BITS = 5
	)
(
	//Inputs
	input start,
	input clk,
	input reset,
	input [DATA_LENGTH - 1 : 0]less_two_significant_bits,
	input algorithm_finished,
	//Outputs
	output [DATA_LENGTH - 1 : 0]ASN,
	output count_init,
	output ready,
	output flush,
	output initial_flag,
	output continuous_flag
);
enum logic[2:0] {IDLE, BEGIN, SRN, READY, FLUSH} state, next_state;
bit flush_bit;
bit ready_bit;
bit initial_flag_bit;
bit continuous_flag_bit;
bit count_init_wire;
logic [DATA_LENGTH - 1 : 0]ASN_bit;
always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		state <= IDLE;
	else
		state <= next_state;
end
	
always_comb
begin
	case(state)
		IDLE:
		begin
			if(start)
				next_state = BEGIN;
			else
				next_state = IDLE;
		end
		BEGIN:
			next_state = SRN;
		SRN:
		begin
			if(algorithm_finished == 1)
				next_state = READY;
			else
				next_state = SRN;
		end
		READY:
			if(start == 0)
				next_state = FLUSH;
			else
				next_state = READY;
		FLUSH:
			next_state = IDLE;
		endcase
end

//Outputs assignations, combinational process
always_comb
begin
initial_flag_bit = 0;
continuous_flag_bit = 0;
flush_bit = 0;
ready_bit = 0;
ASN_bit = 0;
count_init_wire = 0;
	case(state)
		IDLE:
		begin
		end
		BEGIN:
		begin
			initial_flag_bit = 1;
			count_init_wire = 1;
		end
		SRN:
		begin
			count_init_wire = 1;
			continuous_flag_bit = 1;
			if(less_two_significant_bits == 2'b00 || less_two_significant_bits == 2'b11)
				ASN_bit = 2'b00;
			else if (less_two_significant_bits == 2'b01)
				ASN_bit = 2'b01;
			else if(less_two_significant_bits == 2'b10)
				ASN_bit = 2'b10;
		end
		READY:
		begin
			ready_bit = 1;
		end
		FLUSH:
		begin
			flush_bit = 1;
		end
		default:
		begin
			initial_flag_bit = 0;
			continuous_flag_bit = 0;
			flush_bit = 0;
			ready_bit = 0;
			ASN_bit = 0;
			count_init_wire = 0;
		end
	endcase
	
end

assign initial_flag = initial_flag_bit;
assign continuous_flag = continuous_flag_bit;
assign flush = flush_bit;
assign ready = ready_bit;
assign ASN = ASN_bit;
assign count_init = count_init_wire;
endmodule