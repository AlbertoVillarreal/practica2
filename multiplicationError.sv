module multiplicationError
#(
	parameter DATA_LENGTH_32 = 32,
	parameter DATA_LENGTH_16 = 16
)
(
	input signed [DATA_LENGTH_32 - 1 : 0]data,
	output [DATA_LENGTH_16 - 1 : 0] data_out,
	output bit error
);
	logic [DATA_LENGTH_16 - 1 : 0]data_out_wire;
	bit error_wire;
always_comb
begin
	if(data > 32766 || data < -32767)
	begin
		data_out_wire = 32767;
		error_wire = 1;
	end
	else
	begin
		data_out_wire = data;
		error_wire = 0;
	end
end
assign data_out = data_out_wire;
assign error = error_wire;
endmodule