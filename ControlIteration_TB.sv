timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module ControlIteration_TB;
	//Inputs
	logic [5 - 1 : 0]Counter_log = 0;
	
	//Outputs
	logic [5 - 1 : 0]Out_log;
	
ControlIteration
DUT
(
	//Inputs
	.Counter(Counter_log),
	
	//Outputs
	.Out(Out_log)
	
);


/*initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  */
  
  
  /*********************************************************/
initial begin // reset generator
	#4 Counter_log = 1;
	#2 Counter_log = 2;
	#2 Counter_log = 3;
	#2 Counter_log = 4;
	#2 Counter_log = 5;
	#2 Counter_log = 6;
	#2 Counter_log = 7;
	#2 Counter_log = 8;
	#2 Counter_log = 9;
	#2 Counter_log = 10;
	#2 Counter_log = 11;
	#2 Counter_log = 12;
	#2 Counter_log = 13;
	#2 Counter_log = 14;
	#2 Counter_log = 15;
	
end

endmodule