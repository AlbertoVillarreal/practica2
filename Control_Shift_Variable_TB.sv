timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module Control_Shift_Variable_TB;
	//Inputs
	logic [16 - 1 : 0]data_log = 15;
	logic [16 - 11: 0]Nshifts_log = 0;
	bit right_left_flag_log  = 0;
	
	//Outputs
	logic [16 - 1 : 0]shifted_data_log;
	
Control_Shift_Variable
DUT
(
	//Inputs
	.data(data_log),
	.Nshifts(Nshifts_log),
	.right_left_flag(right_left_flag_log),
	
	//Outputs
	.shifted_data(shifted_data_log)
	
);


/*initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  */
  
  
  /*********************************************************/
initial begin // reset generator
	#4 Nshifts_log = 1;
	#4 Nshifts_log = 2;
	#4 Nshifts_log = 3;
	
end

endmodule