/********************************************************************
* Name:
*	BinarioBCD.sv
*
* Description:
* 	This module is a structural decoder
*
* Inputs:
*	data_input: 							Binary data from switches in binary format.
*
* Outputs:
* 	units_display_output: 						Hundreds in 7 segments.
* 	tens_display_output: 							Tens in 7 segments.
* 	hundreds_display_output: 							Units in 7 segments.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	3/2/2018 
*
*********************************************************************/
module Decodificador
#(
	parameter word_length = 16
)
(
	//Input ports
	input [word_length - 1 : 0] data_input,
	
	//Output ports
	output [6 : 0] units_display_output,
	output [6 : 0] tens_display_output,
	output [6 : 0] hundreds_display_output,
	output [6 : 0] thousands_units_display_output,
	output [6 : 0] thousands_tens_display_output,
	output sign
	
);

wire [word_length - 1 : 0] output_valorAbsoluto_wire;	//absolute value from the inpu
wire [3 : 0] units_output_wire;								//units in BCD
wire [3 : 0] tens_output_wire;								//tens in BCD
wire [3 : 0] hundreds_output_wire;							//hundreds in BCD
wire [3 : 0] thousands_units_output_wire;					//units of thousands in BCD
wire [3 : 0] thousands_tens_output_wire;					//tens of thousands in BCD
wire [6 : 0] units_display_wire;								//units in seven segments
wire [6 : 0] tens_display_wire;								//tens in seven segments
wire [6 : 0] hundreds_display_wire;							//hundreds in seven segments
wire [6 : 0] thousands_units_display_wire;				//units of thousands in seven segments
wire [6 : 0] thousands_tens_display_wire;					//tens of thousands in seven segments
bit signo_valorAbsoluto_wire;

ValorAbsoluto
This_is_ValorAbsoluto
(
	//Input ports
	.entrada(data_input),
	//Output ports
	.salida(output_valorAbsoluto_wire),
	.signo(signo_valorAbsoluto_wire)
);


BinarioBCD
This_is_BinarioBCD
(
	//Input ports
	.data_input(output_valorAbsoluto_wire),
	//Output ports
	.units_output(units_output_wire),
	.tens_output(tens_output_wire),
	.hundreds_output(hundreds_output_wire),
	.thousands_units_output(thousands_units_output_wire),
	.thousands_tens_output(thousands_tens_output_wire)
);

BCD_7_segmentos
Uints_Segments
(
		//Input ports
	.num(units_output_wire),
	
	//Output ports
	.display_output(units_display_wire)
);

BCD_7_segmentos
Tens_Segments
(
		//Input ports
	.num(tens_output_wire),
	
	//Output ports
	.display_output(tens_display_wire)
);

BCD_7_segmentos
Hundreds_Segments
(
		//Input ports
	.num(hundreds_output_wire),
	
	//Output ports
	.display_output(hundreds_display_wire)
);

BCD_7_segmentos
thousands_units_Segments
(
		//Input ports
	.num(thousands_units_output_wire),
	
	//Output ports
	.display_output(thousands_units_display_wire)
);

BCD_7_segmentos
thousands_tens_Segments
(
		//Input ports
	.num(thousands_tens_output_wire),
	
	//Output ports
	.display_output(thousands_tens_display_wire)
);



assign units_display_output = units_display_wire;
assign tens_display_output = tens_display_wire;
assign hundreds_display_output = hundreds_display_wire;
assign thousands_units_display_output = thousands_units_display_wire;
assign thousands_tens_display_output = thousands_tens_display_wire;
assign sign = signo_valorAbsoluto_wire;

endmodule

