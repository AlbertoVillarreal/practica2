module DivisionNonRestoring
#(
	parameter DATA_LENGTH_16 = 16,
	parameter DATA_LENGTH_32 = 32
)
(
	//Inputs
	input [DATA_LENGTH_16 - 1 : 0]dividend,
	input [DATA_LENGTH_16 - 1 : 0]divisor,
	input [DATA_LENGTH_16 - 1 : 0]adder_in,
	input clk,
	input reset,
	input start,

	//Outputs
	output [DATA_LENGTH_16 - 1 : 0]quotient,
	output [DATA_LENGTH_16 - 1 : 0]remainder,
	output [DATA_LENGTH_16 - 1 : 0]adder_out,
	output bit ready,
	output bit error,
	output bit SOR
	);

//Shift wires
logic[DATA_LENGTH_32 - 1 : 0] initial_shifted_data_wire;
logic[DATA_LENGTH_32 - 1 : 0] continuous_shift_wire;
logic[DATA_LENGTH_32 - 1 : 0] final_step_wire;

//Register wires
logic[DATA_LENGTH_16 - 1 : 0] data_to_adder_out_wire;
logic[DATA_LENGTH_16 - 1 : 0] data_to_concatenation_wire;
logic[DATA_LENGTH_32 - 1 : 0] data_to_final_wire;
logic[DATA_LENGTH_16 - 1 : 0] register_adder_wire;
logic[DATA_LENGTH_16 - 1 : 0] register_normal_wire;


//Concatenation wires
logic[DATA_LENGTH_32 - 1 : 0] data_concatenated_wire;

//IPN Wires
logic[DATA_LENGTH_32 - 1 : 0] data_lsb_modified_wire; 
bit new_data_previous_data_flag_wire;

//Control wires
bit initial_flag_wire;
bit continuous_flag_wire;
bit ready_flag_wire;
bit flush_flag_wire;
bit count_init_wire;
bit final_condition_flag_wire;
bit SOR_wire;

//Counter wires
bit algorithm_finished_wire;
logic[4:0] counter_wire;
//Multiplexor wires
logic[DATA_LENGTH_32 - 1 : 0] Initial_mux_data_wire;
logic[DATA_LENGTH_32 - 1 : 0] Final_mux_data_wire;
logic[DATA_LENGTH_32 - 1 : 0] error_normal_mux_wire;
//Error wires
bit error_wire;
logic[DATA_LENGTH_16 - 1 : 0] remainder_error_wire;
logic[DATA_LENGTH_16 - 1 : 0] quotient_error_wire;

/////////////////////////////////
/////////////////////////////////
division_error
#(
	.DATA_LENGTH(16)	
)
ERROR_DIVISION
(
	.dividend(dividend),
	.divisor(divisor),
	
	.error(error_wire),
	.remainder(remainder_error_wire),
	.quotient(quotient_error_wire)
);

///////////////////////////
///////////////////////////
Multiplexer2to1
#(
	.NBits(32)
)
MUX_INIT_DIVISION
(
	.Selector(continuous_flag_wire),
	.MUX_Data0({16'b0,dividend}),
	.MUX_Data1(data_lsb_modified_wire),
	
	.MUX_Output(Initial_mux_data_wire)

);

////////////////////////////////////////////////////
///////////////////////////////////////////////////////
Register_Division_with_clock_enable
#(
	.Word_Length(16)
)
ADDERREGISTER_DIVISION
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(Initial_mux_data_wire[31:16]),
	.final_condition_flag(((final_condition_flag_wire == 1) && (Initial_mux_data_wire[31] == 1)) || ready_flag_wire),
	.flush(flush_flag_wire),
	// Output Ports
	.Data_Output(register_adder_wire)
);

Register_Division_with_clock_enable
#(
	.Word_Length(16)
)
NORMALREGISTER_DIVISION
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(Initial_mux_data_wire[15:0]),
	.final_condition_flag(final_condition_flag_wire || ready_flag_wire),
	.flush(flush_flag_wire),
	// Output Ports
	.Data_Output(register_normal_wire)
);
/************************************************************************************************
Shift. 
This shift is going to do a shift to the left and it's done until the algorithm is over.
Inputs:
	[data]: 					it´s the concatenation of the adder and the less significant part.
	[right_left_flag]: 	the shift it's gooing to be to the left in this case.

Outputs:
	[shifted_data]: 		Data shifted by one to the left.
*/
shift
#(
	.entrada(32),
	.left(0),
	.Nshifts(1)
)
CONTINUOUS_SHIFT_DIVISION
(
	//Inputs
	.data({register_adder_wire,register_normal_wire}),
	.right_left_flag(0),
	//Outputs
	.shifted_data(continuous_shift_wire)
);

///////////////////////////7
///////////////////////////7
Multiplexer2to1
#(
	.NBits(32)
)
MUX_FINAL_DIVISION
(
	.Selector(final_condition_flag_wire),
	.MUX_Data0(continuous_shift_wire),
	.MUX_Data1({register_adder_wire,register_normal_wire}),
	
	.MUX_Output(Final_mux_data_wire)

);
/************************************************************************************************
Concatenation. 
This shift is going to do a shift to the left and it's going to go to the register.
Inputs:
	[data_1]: 					Data that comes from the register, it's the left part of the variable.
	[data_2]: 					Data that comes from the adder.

Outputs:
	[data_concatenated]: 	Previous data concatenated.
*/
Concatenation
#(
	.DATA_LENGTH(16),
	.DATA_CONCATENATED_LENGTH(32),
	.DATA_LENGTH_DATA2(16)
)
CONCATENATION_DIVISION
(
	//Inputs
	.data_1(adder_in),
	.data_2(Final_mux_data_wire[15:0]),
	//Outputs
	.data_concatenated(data_concatenated_wire)
);
/************************************************************************************************
P_negative_or_positive. 
This module modify the less significant bit of the data input, this is done because of the algorithm.
Inputs:
	[data]: 			Data that comes from the concatenation module.
Outputs:
	[data_out]: 	Data with less siginificant bit modified.
*/
P_negative_or_positive
#(
	.DATA_LENGTH(32)
)
MODIFIEDBIT_DIVISION
(
	//Inputs
	.data(data_concatenated_wire),
	//Outputs
	.data_out(data_lsb_modified_wire)

);


/************************************************************************************************
Register. 
This register is going to recive the first shifted variable and the second one is the continuos shifting,
i'ts going to save the previous shifted value and it's going to send the left part of the variable to the 
adder and the rigth part to the concatenation.
Inputs:
	[clk]: 								it´s the concatenation of the divisor and dividend.
	[reset]: 							the shift it's gooing to be to the left in this case.
	[start]:								Flag wich indicates the beggining of the state machine.
	[AOS]:								Bit to see if the algorithm needs an adition or a substraction.
	[algorithm_finished]:			Flag wich indicates the end of the state machine.

Outputs:
	[initial_flag]:					Flag wich indicates the initial data of the register.
	[continuous_flag]:				Flag wich indicates the continuous data of the register.
	[ready_flag]:						Flag wich indicates the data is ready.	
	[flush_flag]:						Flag wich indicates the flush of the register
	[count_init]:						Flag wich indicates te enable of the counter
	[SOR]:								Flag wich indicates if the adder is going to do an adition or a substraction
*/
Division_control
CONTROL_DIVISION
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.AOS(register_adder_wire[15]),
	.algorithm_finished(algorithm_finished_wire),
	.counter(counter_wire),
	.error(error_wire),
	//Outputs
	.initial_flag(initial_flag_wire),
	.continuous_flag(continuous_flag_wire),
	.ready_flag(ready_flag_wire),
	.flush_flag(flush_flag_wire),
	.count_init(count_init_wire),
	.final_condition_flag(final_condition_flag_wire),
	.SOR(SOR_wire)
);

CounterWithFunction
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(18),
	.NBITS_FOR_COUNTER(CeilLog2(18))
)
COUNTER_DIVISION
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(count_init_wire),
	
	// Output Ports
	.flag(algorithm_finished_wire),
	.CountOut(counter_wire) 
);

///////////////////////////7
///////////////////////////7
Multiplexer2to1
#(
	.NBits(32)
)
MUX_ERROR_NORMAL
(
	.Selector(error_wire),
	.MUX_Data0({register_adder_wire,register_normal_wire}),
	.MUX_Data1({remainder_error_wire,quotient_error_wire}),
	
	.MUX_Output(error_normal_mux_wire)

);

assign adder_out = Final_mux_data_wire[31:16];
assign quotient = error_normal_mux_wire[15:0];
assign remainder = error_normal_mux_wire[31:16];
assign ready = ready_flag_wire;
assign SOR = SOR_wire;
assign error = error_wire;
//////////////////////////Compilation directives//////////////////////////
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
endmodule