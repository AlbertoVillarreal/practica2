timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module OR_Module_TB;
	//Input
	bit sign_log = 0;
	logic [16 - 1 : 0]data_in_0_log;
	logic [16 - 1 : 0]data_in_1_log;
	
	//Output
	logic [16 - 1 : 0]data_out_log;
	
OR_Module
DUT
(
	.sign(sign_log),
	.data_in_0(data_in_0_log),
	.data_in_1(data_in_1_log),
	
	//Output
	.data_out(data_out_log)
	
);


/*initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  */
  
  
  /*********************************************************/
initial begin // reset generator
	#4 data_in_0_log = 4;
	#0 data_in_1_log = 8;
	#4 data_in_0_log = 32;
	#0 data_in_1_log = 2;
	
end

endmodule