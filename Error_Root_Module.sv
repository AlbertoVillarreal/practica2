module Error_Root_Module
#(
	parameter Nbits = 16
)
(
	//Input
	input [Nbits - 1 : 0]X,
	
	//Outputs
	output error
);

logic error_log;

always_comb
begin

	if(X[Nbits-1] == 0)
		error_log = 0;
	else
		error_log = 1;

end

assign error = error_log;
endmodule