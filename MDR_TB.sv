timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module MDR_TB;

	//Inputs
	logic clk = 0;
	logic reset = 0;
	logic start = 0;
	logic load = 0;
	logic use_last_data = 0;
	logic [16 - 15 : 0]Opcode = 1;
	logic [16 - 1 : 0]Data_Input = 0;
	
	//Outputs
	logic ready_log;
	logic error_log;
	logic sign_log;
	logic loadX_LED_log;
	logic loadY_LED_log;
	logic [16 - 1 : 0]result_log;
	logic [16 - 1 : 0]remainder_log;
	logic [16 - 10 : 0]units_display_result_log;
	logic [16 - 10 : 0]tens_display_result_log;
	logic [16 - 10 : 0]hundreds_display_result_log;
	logic [16 - 10 : 0]thousands_units_display_result_log;
	logic [16 - 10 : 0]thousands_tens_display_result_log;
	
MDR	
DUT
(

	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.load(load),
	.use_last_data(use_last_data),
	.Opcode(Opcode),
	.Data_Input(Data_Input),
	
	//Outputs
	.ready(ready_log),
	.error(error_log),
	.sign(sign_log),
	.loadX_LED(loadX_LED_log),
	.loadY_LED(loadY_LED_log),
	.result(result_log),
	.remainder(remainder_log),
	.units_display_result(units_display_result_log),
	.tens_display_result(tens_display_result_log),
	.hundreds_display_result(hundreds_display_result_log),
	.thousands_units_display_result(thousands_units_display_result_log),
	.thousands_tens_display_result(thousands_tens_display_result_log)
	
);


initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  
    /*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#6 reset = 1;
	#4 start = 1;
	#8 Data_Input = -32000;
	#8 load = 1;
	#8 load = 0;
	#8 Data_Input = 2;
	#8 load = 1;
	#8 load = 0;
	#100 start = 0;
	
	#4 start = 1;
	#8 Data_Input = -4;
	#8 load = 1;
	#8 load = 0;
	#8 Data_Input = 1;
	#8 load = 1;
	#8 load = 0;
	#100 start = 0;
	
	#4 start = 1;
	#8 Data_Input = 15;
	#8 load = 1;
	#8 load = 0;
	#8 Data_Input = 8;
	#8 load = 1;
	#8 load = 0;
	#100 start = 0;
end

endmodule