/**/
module Raiz
#(
	parameter Nbits = 16
)
(
	//Inputs
	input bit clk,
	input bit reset,
	input bit start,
	input bit finish_counting,
	input [Nbits - 13: 0]counter,
	input [Nbits - 1 : 0]data_in_Adder,
	input [Nbits - 1 : 0]data_in_X,
	
	//Outputs
	output bit ready,
	output bit start_counting,
	output bit error,
	output [Nbits - 1 : 0]remainder,
	output [Nbits - 1 : 0]result,
	output [Nbits - 1 : 0]root_final_result,
	output [Nbits - 1 : 0]remainder_final_result
);
	
	bit error_flag;
	
	bit enable_R_Register;
	bit enable_D_Register;
	bit enable_Q_Register;
	bit for_cycle;
	bit flush_bit;
	bit ready_log;
	bit MUX_MSBSelector_to_MUXFeedbackSelector;							//Salida de multiplexor de selector de bit mas significativo a selector de multiplexor de retroalimentacion
	bit ORForCycle_to_SelectorMUXLastOperation;  						//Salida de compuerta OR de ciclo for a selector de multiplexor de ultima operacion
	logic [Nbits - 12: 0]ControlIteration_to_ControlShiftVariable;	//Control de iteracion a shift de control variable (i << 1)
	logic [Nbits - 1 : 0]R_register_to_ShiftRemainder;					//Registro de Residuo a shift de residuo
	logic [Nbits - 1 : 0]ShiftRemainder_to_ORRemainder;				//Shift de residuo a OR de residuo
	logic [Nbits - 1 : 0]AND_DataX_to_ORRemainder;						//Compuerta AND de dato de entrada X a compuerta OR de residuo
	logic [Nbits - 1 : 0]ORRemainder_to_MUXRemainder;					//Compuerta OR de residuo a Multiplexor de residuo
	logic [Nbits - 1 : 0]MUXRemainder_to_Adder;							//Conectar a la salida de remainder (Va al sumador)
	logic [Nbits - 1 : 0]D_register_to_ControlShift;					//Registro de dato X a shift de control variable (i << 1)
	logic [Nbits - 1 : 0]ControlShiftVariable_to_AND;					//Shift de control variable a compuerta AND
	logic [Nbits - 1 : 0]Q_register_to_shift_modules;					//Registro de raiz a 2 modulos de corrimiento (<<2) y (<<1)
	logic [Nbits - 1 : 0]ShiftRootRemainder_to_OR_Sign_Selector;	//Registro de corrimiento RootRemainder(<<2) a compuerta OR con selector de signo
	logic [Nbits - 1 : 0]MUX_to_OR_Sign_Selector;						//Multiplexor 1 o 3 a compuerta OR con selector de signo
	logic [Nbits - 1 : 0]OR_Sign_Selector_to_MUX_Root_For_Cycle;	//OR con selector de signo a multiplexor de residuo para ciclo for
	logic [Nbits - 1 : 0]ShiftRootFeedback_to_ORRoot_Feedback;		//Registro de corrimiento RootFeedback(<<1) a compuerta OR feedback
	logic [Nbits - 1 : 0]MUX_QFeedback_to_ORRoot_Feedback;			//Multiplexor de retroalimentacion de raiz a compuerta OR feedback
	logic [Nbits - 1 : 0]ORRoot_Feedback_to_MUX_Root;					//Compuerta OR feedback a multiplexor de raiz
	logic [Nbits - 1 : 0]MUXRootForCycle_to_MUXLastOperation;		//Multiplexor de raiz a multiplexor de ultima operacion
	logic [Nbits - 1 : 0]MUXLastOperation_to_Adder;						//Multiplexor de ultima operacion a adder(Va al sumador)

	

	
Error_Root_Module
#(
	.Nbits(16)
)
ERROR_ROOT_MODULE
(
	//Input
	.X(data_in_X),
	
	//Outputs
	.error(error_flag)
);

/************************************************************************************************
	Modulo de control para la raiz. 
	Entradas:
		[start]: Modulo de control de raiz
		[finish_counting]: Retroalimentacion de adder
	
	Salidas:
		[enable_Dregister]: 
		[enable_Rregister]: 
		[start_counting]: 
		[enable_Qregister]: 
		[for_cycle]: 
		[ready]: 
		[flush]: 
*/	
Control_Root_Module
ROOT_CONTROL_STATE_MACHINE
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),
	.finish_counting(finish_counting),
	.error(error_flag),
	
	//Outputs
	.enable_Dregister(enable_D_Register),
	.enable_Rregister(enable_R_Register),
	.start_counting(start_counting),
	.enable_Qregister(enable_Q_Register),
	.for_cycle(for_cycle),
	.ready(ready_log),
	.flush(flush_bit)
	
);	
	
/************************************************************************************************
	Registro de residuo. 
	Guardamos dato de retroalimentacion del adder.
	Entradas:
		[enable]: Modulo de control de raiz
		[Data_Input]: Retroalimentacion de adder
	
	Salidas:
		[Data_Output]: Modulo de corrimiento <<2 y al multiplexor de residuo MUX_Data0
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
REMAINDER_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(enable_R_Register),
	.sync_reset(flush_bit),
	.Data_Input(data_in_Adder),

	// Output Ports
	.Data_Output(R_register_to_ShiftRemainder)
);

/************************************************************************************************
	Registro de corrimiento <<2. 
	Recorre 2 bits hacia la izquierda el residuo.
	Entradas:
		[data]: Registro de residuo
		[right_left_flag]: Bandera para direccion de corrimiento
	
	Salidas:
		[shifted_data]: Compuerta OR de residuo
*/
shift
#(
	.entrada(Nbits),
	.left(1),
	.Nshifts(2)
)
SHIFT_REMAINDER
(
	//Inputs
	.data(R_register_to_ShiftRemainder),
	.right_left_flag(1),
	//Outputs
	.shifted_data(ShiftRemainder_to_ORRemainder)
);

/************************************************************************************************
	Compuerta OR. 
	Hacemos operacion logica entre el residuo y el numero ingresado por el usuario
	Entradas:
		[sign]: Signo del dato de salida. 0 -> ~(data_out) | 1 -> data_out
		[data_in_0]: Modulo de corrimiento de residuo
		[data_in_1]: Compuerta AND de dato X
	
	Salidas:
		[data_out]: Multiplexor Cilco For de residuo, MUX_Data1
*/
OR_Module
#(
	.NBits(Nbits),
	.Negativo(1)
)
OR_REMAINDER_DATAX
(
	//Input
	.sign(1),
	.data_in_0(ShiftRemainder_to_ORRemainder),
	.data_in_1(AND_DataX_to_ORRemainder),
	
	//Output
	.data_out(ORRemainder_to_MUXRemainder)
);

/************************************************************************************************
	Multiplexor 2 a 1 de residuo. 
	Se elige entre 2 entradas, la salida del registro de residuo o la salida de la compuerta OR
	de residuo. Como selector se utliliza señal de control de ciclo for.
	Entradas:
		[Selector]: Senial de control para indicar cuando se termina ciclo.
		[MUX_Data0]: Registro de residuo
		[MUX_Data1]: Compuerta OR de residuo
	
	Salidas:
		[MUX_Output]: Salida al Adder
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_REMAINDER
(
	//Inputs
	.Selector(for_cycle),
	.MUX_Data0(R_register_to_ShiftRemainder),
	.MUX_Data1(ORRemainder_to_MUXRemainder),
	
	//Outputs
	.MUX_Output(MUXRemainder_to_Adder)//Salida para el adder

);

/************************************************************************************************
	Multiplexor 2 a 1 que muestra el resultado final del residuo.
	Entradas:
		[Selector]: Senial de ready.
		[MUX_Data0]: 0
		[MUX_Data1]: Salida de multiplexor de residuo.
	
	Salidas:
		[MUX_Output]: Salida a resultado final de residuo.
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_REMAINDER_FINAL_RESULT
(
	//Inputs
	.Selector(ready_log), 
	.MUX_Data0(16'b0),
	.MUX_Data1(MUXRemainder_to_Adder),
	
	//Outputs
	.MUX_Output(remainder_final_result) //Salida para tomar el valor de Q

);

/************************************************************************************************
	Registro del dato de entrada X (numero a sacar raiz). 
	Guardamos dato que se ingresa al sistema.
	Entradas:
		[enable]: Modulo de control de raiz. (Se activa al momento de inicial la raiz)
		[Data_Input]: Dato de entrada X
	
	Salidas:
		[Data_Output]: Modulo de corriemiento de control (i << 1) 
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
DATA_X_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(enable_D_Register),
	.sync_reset(flush_bit),
	.Data_Input(data_in_X),

	// Output Ports
	.Data_Output(D_register_to_ControlShift)
);

/************************************************************************************************
	Registro de corrimiento que puede hacer un numero de corrimientos variable en ambas direcciones. 
	El numero de corrimientos cambia de acuerdo al numero iteracion.
	Entradas:
		[data]: Registro de dato X
		[Nshifts]: Modulo de control (5 bits)
		[right_left_flag]: En 0 para que haga corrimiento a la derecha
	
	Salidas:
		[shifted_data]: Compuerta AND de dato X, data_in_0 
*/
Control_Shift_Variable
#(
	.entrada(Nbits),
	.left(1)
)
CONTROL_SHIFT_VARIABLE
(
	//Inputs
	.data(D_register_to_ControlShift),
	.Nshifts(ControlIteration_to_ControlShiftVariable),
	.right_left_flag(0),
	
	//Outputs
	.shifted_data(ControlShiftVariable_to_AND)
);

/************************************************************************************************
	Compuerta AND. Hace operacion logica entre el dato de entrada X recorrido y una cosntante 3.
	Entradas:
		[data_in_0]: Corrimiento de control variable
		[data_in_1]: 3 (Constante)
	
	Salidas:
		[data_out]: Compuerta OR de residuo, data_in_1
*/
AND_Module
#(
	.Nbits(Nbits)
)
AND_DATA_X
(
	//Inputs
	.data_in_0(ControlShiftVariable_to_AND),
	.data_in_1(16'd3),
	
	//Outputs
	.data_out(AND_DataX_to_ORRemainder)
);

/************************************************************************************************
	Control de iteracion para el registro de corrimiento variable
	Entradas:
		[Counter]: Counter del sistema. Numero de iteracion i.
	
	Salidas:
		[Out]: Numero de corrimientos del registro de corrimiento variable.
*/
ControlIteration
#(
	.Nbits((Nbits-12))
)
CONTROL_ITERATION
(
	//Inputs
	.Counter(counter),
	
	//Outputs
	.Out(ControlIteration_to_ControlShiftVariable)
);



/************************************************************************************************
	Registro de resultado (Q). Guardamos el dato de las iteraciones para obtener la raiz.
	Entradas:
		[enable]: Modulo de control de raiz. (Se activa un ciclo despues del registro R)
		[Data_Input]: Compuerta OR con el calculo de Q
	
	Salidas:
		[Data_Output]: Modulo de corrimiento (<< 2) y (<< 1) 
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
ROOT_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(enable_Q_Register),
	.sync_reset(flush_bit),
	.Data_Input(ORRoot_Feedback_to_MUX_Root),

	// Output Ports
	.Data_Output(Q_register_to_shift_modules)
);

/************************************************************************************************
	Registro de corrimiento <<2. 
	Recorre 2 bits hacia la izquierda el dato de raiz.
	Entradas:
		[data]: Registro de raiz (Q)
		[right_left_flag]: Bandera para direccion de corrimiento
	
	Salidas:
		[shifted_data]: Compuerta OR de raiz con selector de signo, data_in_0
*/
shift
#(
	.entrada(Nbits),
	.left(1),
	.Nshifts(2)
)
SHIFT_ROOT_REMAINDER
(
	//Inputs
	.data(Q_register_to_shift_modules),
	.right_left_flag(1),
	//Outputs
	.shifted_data(ShiftRootRemainder_to_OR_Sign_Selector)
);
	
/************************************************************************************************
	Multiplexor 2 a 1 de raiz. 
	Se elige entre 2 entradas, 1 o 3. Como selector se utliliza el bit de signo del residuo.
	Entradas:
		[Selector]: Bit de signo de residuo.
		[MUX_Data0]: 1
		[MUX_Data1]: 3
	
	Salidas:
		[MUX_Output]: Compuerta OR de raiz con selector de signo, data_in_1
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_ROOT_REMAINDER
(
	//Inputs
	.Selector(R_register_to_ShiftRemainder[Nbits - 1]), //Bit mas significativo
	.MUX_Data0(16'b1),
	.MUX_Data1(16'd3),
	
	//Outputs
	.MUX_Output(MUX_to_OR_Sign_Selector)

);

/************************************************************************************************
	Compuerta OR. 
	Hacemos operacion logica entre la raiz recorrida (<<2) y el dato de salida del multiplexor (1 o 3).
	Dependiendo del signo es si tenemos una salida positiva o negativa.
	Entradas:
		[sign]: Bit de signo del residuo. 0 -> -data_out | 1 -> data_out
		[data_in_0]: Registro de corrimiento (<<2) de raiz
		[data_in_1]: Salida de multiplexor de raiz
	
	Salidas:
		[data_out]: Multiplexor Cilco For de raiz, MUX_Data1
*/
OR_Module
#(
	.NBits(Nbits),
	.Negativo(1)
)
OR_ROOT_SIGN_SELECTOR
(
	//Input
	.sign(R_register_to_ShiftRemainder[Nbits - 1]),
	.data_in_0(ShiftRootRemainder_to_OR_Sign_Selector),
	.data_in_1(MUX_to_OR_Sign_Selector),
	
	//Output
	.data_out(OR_Sign_Selector_to_MUX_Root_For_Cycle)
);

/************************************************************************************************
	Registro de corrimiento <<1. 
	Recorre 2 bits hacia la izquierda el dato de raiz.
	Entradas:
		[data]: Registro de raiz (Q)
		[right_left_flag]: Bandera para direccion de corrimiento
	
	Salidas:
		[shifted_data]: Compuerta OR de raiz sin selector de signo, data_in_0
*/
shift
#(
	.entrada(Nbits),
	.left(1),
	.Nshifts(1)
)
SHIFT_ROOT_FEEDBACK
(
	//Inputs
	.data(Q_register_to_shift_modules),
	.right_left_flag(1),
	//Outputs
	.shifted_data(ShiftRootFeedback_to_ORRoot_Feedback)
);

/************************************************************************************************
	Multiplexor 2 a 1 de selector de bit mas significativo. 
	Se elige entre 2 entradas, bit mas significativo. Como selector se utliliza la senial de control ciclo for.
	Entradas:
		[Selector]: senial de control ciclo for
		[MUX_Data0]: ~R[N-1]
		[MUX_Data1]: R[N-1]
	
	Salidas:
		[MUX_Output]: Selector de multiplexor de retroalimentacion de raiz
*/
Multiplexer2to1
#(
	.NBits(1)
)
MUX_MSB_SELECTOR
(
	//Inputs
	.Selector(for_cycle), 
	.MUX_Data0(~R_register_to_ShiftRemainder[Nbits - 1]),
	.MUX_Data1(data_in_Adder[Nbits - 1]),
	
	//Outputs
	.MUX_Output(MUX_MSBSelector_to_MUXFeedbackSelector)

);

/************************************************************************************************
	Multiplexor 2 a 1 de retroalimentacion raiz. 
	Se elige entre 2 entradas, 1 o 0. Como selector se utliliza el bit de signo del residuo.
	Entradas:
		[Selector]: Multiplexor de selector de bit mas significativo.
		[MUX_Data0]: 1
		[MUX_Data1]: 0
	
	Salidas:
		[MUX_Output]: Compuerta OR de retroalimentacion de raiz sin selector de signo, data_in_1
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_ROOT_FEEDBACK
(
	//Inputs
	.Selector(MUX_MSBSelector_to_MUXFeedbackSelector), //Bit mas significativo
	.MUX_Data0(16'b1),
	.MUX_Data1(16'b0),
	
	//Outputs
	.MUX_Output(MUX_QFeedback_to_ORRoot_Feedback)

);

/************************************************************************************************
	Compuerta OR. 
	Hacemos operacion logica entre la raiz recorrida (<<1) y el dato de salida del multiplexor (1 o 0).
	Entradas:
		[sign]: Bit de signo del residuo. 0 -> -data_out | 1 -> data_out
		[data_in_0]: Registro de corrimiento (<<1) de retroalimentacion de raiz
		[data_in_1]: Salida de multiplexor de retroalimentacion de raiz
	
	Salidas:
		[data_out]: Multiplexor Cilco For de raiz, MUX_Data0
*/
OR_Module
#(
	.NBits(Nbits),
	.Negativo(1)
)
OR_ROOT_FEEDBACK
(
	//Input
	.sign(1),
	.data_in_0(ShiftRootFeedback_to_ORRoot_Feedback),
	.data_in_1(MUX_QFeedback_to_ORRoot_Feedback),
	
	//Output
	.data_out(ORRoot_Feedback_to_MUX_Root)
);

/************************************************************************************************
	Multiplexor 2 a 1 de retroalimentacion raiz. 
	Se elige entre 2 entradas, Compuerta OR de calculo para residuo o Compuerta OR de retroalimentacion de valor de raiz.
	Como selector se utliliza la senial de control de ciclo for.
	Entradas:
		[Selector]: Senial de control de ciclo for.
		[MUX_Data0]: Compuerta OR de calculo para residuo
		[MUX_Data1]: Compuerta OR de retroalimentacion de valor de raiz
	
	Salidas:
		[MUX_Output]: Multiplexor de la ultima operacion (saliendo de ciclo for) para obtener el residuo, MUX_Data1
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_ROOT_FOR_CYCLE
(
	//Inputs
	.Selector(for_cycle), 
	.MUX_Data0(ORRoot_Feedback_to_MUX_Root),
	.MUX_Data1(OR_Sign_Selector_to_MUX_Root_For_Cycle),
	
	//Outputs
	.MUX_Output(MUXRootForCycle_to_MUXLastOperation)

);

/************************************************************************************************
	Multiplexor 2 a 1 de retroalimentacion raiz. 
	Se elige entre 2 entradas, Salida de multiplexor de ciclo for o 0.
	Como selector se utliliza la salida de compuerta OR.
	Entradas:
		[Selector]: Senial de compuerta OR.
		[MUX_Data0]: 0
		[MUX_Data1]: Salida de multiplexor de ciclo for
	
	Salidas:
		[MUX_Output]: Multiplexor de la ultima operacion (saliendo de ciclo for) para obtener el residuo, MUX_Data1
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_ROOT_LAST_OPERATION
(
	//Inputs
	.Selector(ORForCycle_to_SelectorMUXLastOperation), 
	.MUX_Data0(16'b0),
	.MUX_Data1(MUXRootForCycle_to_MUXLastOperation),
	
	//Outputs
	.MUX_Output(MUXLastOperation_to_Adder)//Segunda salida al adder (valor de Q)

);

/************************************************************************************************
	Compuerta OR. 
	Hacemos operacion logica entre la raiz recorrida (<<1) y el dato de salida del multiplexor (1 o 0).
	Entradas:
		[sign]: Bit de signo del residuo. 0 -> -data_out | 1 -> data_out
		[data_in_0]: Senial de control de ciclo for
		[data_in_1]: Bit mas significativo de registro de residuo
	
	Salidas:
		[data_out]: Multiplexor Cilco For de raiz, MUX_Data0
*/
OR_Module
#(
	.NBits(1),
	.Negativo(1)
)
OR_FOR_CYCLE
(
	//Input
	.sign(1),
	.data_in_0(for_cycle),
	.data_in_1(R_register_to_ShiftRemainder[Nbits - 1]),
	
	//Output
	.data_out(ORForCycle_to_SelectorMUXLastOperation)
);

/************************************************************************************************
	Multiplexor 2 a 1 que muestra el resultado de la raiz.
	Entradas:
		[Selector]: Senial de ready.
		[MUX_Data0]: 0
		[MUX_Data1]: Salida de registro de raiz
	
	Salidas:
		[MUX_Output]: Salida a resultado de raiz.
*/
Multiplexer2to1
#(
	.NBits(Nbits)
)
MUX_ROOT_FINAL_RESULT
(
	//Inputs
	.Selector(ready_log), 
	.MUX_Data0(16'b0),
	.MUX_Data1(Q_register_to_shift_modules),
	
	//Outputs
	.MUX_Output(root_final_result) //Salida para tomar el valor de Q

);



assign remainder = MUXRemainder_to_Adder;
assign result = MUXLastOperation_to_Adder;
assign error = error_flag;
assign ready = ready_log;

endmodule