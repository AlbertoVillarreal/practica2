/********************************************************************
* Name:
*	P_negative_or_positive.sv
*
* Description:
* 	This module recives the actual value of the algorithm and we use the most significant bit to determine if the
*	data is positive or negative, if it's negative a 0 is written in the less significant bit of the data and if
* 	it's positive a 1 is written in the less significant bit of the data.
*
* Inputs:
*	data: 						Data with size of 32bits.
*
* Outputs:
* 	data_out: 					Data with the less signifcant bit modified.
*
*
* Authors: 
* 	Miguel Agustin Gonzalez Rivera
*	Alberto Masaru Villarreal Morishige 
*
* Date: 
*	17/3/2018 
*
*********************************************************************/
module P_negative_or_positive
#(
	parameter DATA_LENGTH = 32
)
(
	//Inputs
	input [DATA_LENGTH - 1 : 0]data,
	//Outputs
	output [DATA_LENGTH - 1 : 0]data_out
);

logic [DATA_LENGTH - 1 : 0]data_out_wire;
bit new_data_previous_data_wire;
always_comb
begin
	data_out_wire = data;
 if(data[DATA_LENGTH - 1])
		data_out_wire[0] = 0;
	else
		data_out_wire[0] = 1;
end
assign data_out = data_out_wire;
endmodule