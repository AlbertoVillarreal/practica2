module Multi_Booth
#(
	parameter DATALENGTH = 16,
	parameter DATALENGTH_PRODUCT = 32
)
(
	input [DATALENGTH-1 : 0]multiplier,
	input [DATALENGTH - 1 : 0]multiplicand,
	input [DATALENGTH - 1 : 0]adder,
	input start,
	input clk,
	input reset,
	//Outputs
	output [DATALENGTH - 1 : 0]product,
	output ready,
	output [DATALENGTH - 1 : 0]first_adder_output,
	output [DATALENGTH - 1 : 0]second_adder_output,
	output error
);

//Register wires
logic [DATALENGTH - 1 : 0]data_register_to_adder_wire;
logic [DATALENGTH : 0]data_register_to_concatenation_wire;
logic [DATALENGTH_PRODUCT - 1 : 0]data_out_to_mux_wire;
//Mux wires
logic [DATALENGTH_PRODUCT - 1 : 0]data_final_output_wire;
logic [DATALENGTH - 1 : 0]data_to_adder_wire;
////////////////
logic [DATALENGTH_PRODUCT : 0]Initial_mux_data_wire;

//Concatenated wires
logic [DATALENGTH_PRODUCT : 0]concatenated_data_wire;

//Shift wires
logic [DATALENGTH_PRODUCT : 0]shifted_data_wire;

//Control wires
logic [1:0] muxAdder_selector_wire;
bit ready_flag_wire;
bit flush_flag_wire;
bit initial_flag_wire;
bit continuous_flag_wire;
bit start_counting_wire;
//Counter wires
bit finish_counting_flag_wire;
logic[4 : 0] counter_wire;

//error wires
bit error_wire;
logic [DATALENGTH - 1 : 0] product_error_wire;
/************************************************************************************************
Counter. 
This module counts until it reach 16 this is used to control the state machine of the controller.
Entradas:
	[clk]: 			Clock board.
	[reset]: 		Asynchronous reset.
	[enable]:		indicate the start of the counter.

Salidas:
	[flag]:  		It indicate the end of the counter.
	[CountOut]:		Counter_data.
*/
CounterWithFunction
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(18),
	.NBITS_FOR_COUNTER(CeilLog2(18))
)
MULTI_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(start_counting_wire),
	
	// Output Ports
	.flag(finish_counting_flag_wire),
	.CountOut(counter_wire) 
);

/************************************************************************************************
Multiplication control. 
This module is a control that sends triggers to the modules of this multiplication.
Inputs:
	[start]:								Start flag that indicates the beggining of the algorithm
	[clk]: 								Board clock.
	[reset]: 							Asynchronous reset.
	[less_two_significant_bits]:	Bits that indicates if an adder or a substraction is needed.
	[cont]:								Counter needed to see the states.

Outputs:
	[ASN]: 								Output that sends a signal to the multiplexor to decide what to do with the multiplicand
	[count_init]:						Indicate the start of the counter.
	[ready]: 							Inidicates the finish of the algorithm.
	[flush]:								Clean the register.
	[initial_flag]:					Flag that indicates that the output is the initialization value.
	[continuous_flag]:				Flag that indicates that the output is the value tha is changing constantly.
*/
Multiplication_control
#(
	.DATA_LENGTH(2),
	.MAX_BITS(5)
	)
MULTI_CONTROL
(
	//Inputs
	.start(start),
	.clk(clk),
	.reset(reset),
	.less_two_significant_bits(data_register_to_concatenation_wire[1:0]),
	.algorithm_finished(finish_counting_flag_wire),
	//Outputs
	.ASN(muxAdder_selector_wire),
	.count_init(start_counting_wire),
	.ready(ready_flag_wire),
	.flush(flush_flag_wire),
	.initial_flag(initial_flag_wire),
	.continuous_flag(continuous_flag_wire)
);
///////////////////////////
///////////////////////////
Multiplexer2to1
#(
	.NBits(33)
)
MUX_INIT_DIVISION
(
	.Selector(continuous_flag_wire),
	.MUX_Data0({16'b0,multiplier,1'b0}),
	.MUX_Data1(shifted_data_wire),
	
	.MUX_Output(Initial_mux_data_wire)

);
////////////////////////////////////////////////
///////////////////////////////////////////////
Register_Division_with_clock_enable
#(
	.Word_Length(16)
)
ADDER_REGISTER_MULTIPLICATION
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(Initial_mux_data_wire[32:17]),
	.final_condition_flag(finish_counting_flag_wire || ready_flag_wire),
	.flush(flush_flag_wire),
	// Output Ports
	.Data_Output(data_register_to_adder_wire)
);
////////////////////////////////////////////////
///////////////////////////////////////////////
Register_Division_with_clock_enable
#(
	.Word_Length(17)
)
TO_CONCATENATE_REGISTER_MULTIPLICATION
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.Data_Input(Initial_mux_data_wire[16:0]),
	.final_condition_flag(finish_counting_flag_wire || ready_flag_wire),
	.flush(flush_flag_wire),
	// Output Ports
	.Data_Output(data_register_to_concatenation_wire)
);

/************************************************************************************************
Multiplexor 3 to 1. 
This mux recive from the controler two bits in wich ones it decide if is a substraction, adder or nothing
The value that is going to be sent is the multiplicand.
Entradas:
	[Selector]: 	two bits that comes from the controller.
	[MUX_Data0]: 	It sends a zero.
	[MUX_Data1]:	It sends a positive value.
	[MUX_Data2]:	It sends a negative value

Salidas:
	[MUX_Output]:  Data out.
*/
Multiplexor3to1
#(
	.NBits(16),
	.SELECTOR_BITS(2)
)
MULTI_MULTIPLEXOR3TO1
(
	//Inputs
	.Selector(muxAdder_selector_wire),
	.MUX_Data0(0),
	.MUX_Data1(multiplicand),
	.MUX_Data2(-multiplicand),
	//Outputs
	.MUX_Output(data_to_adder_wire)

);



/************************************************************************************************
Concatenation. 
This module is used to concatenate the multiplicand and the "multiplier" that is continously channging.
Inputs:
	[data_1]: 							variable that is continuosly changing depending of the multiplicand.
	[data_2]: 							multiplier, is continuosly been shifted.
Outputs:
	[data_concatenated]: 			Te previous data concatenated.
*/
Concatenation
#(
	.DATA_LENGTH(16),
	.DATA_CONCATENATED_LENGTH(33),
	.DATA_LENGTH_DATA2(17)
)
MULTI_CONCATENATION
(
	//Inputs
	.data_1(adder),
	.data_2(data_register_to_concatenation_wire),
	//Outputs
	.data_concatenated(concatenated_data_wire)
);
/************************************************************************************************
Shift. 
This shift is done after the concatenation to shift the complete variable.
Entradas:
	[data]: 					Data concatenated that is shifted.
	[right_left_flag]: 	the shift it's gooing to be to the right in this case.

Salidas:
	[shifted_data]:  Data shifted by one to the right.
*/
shift
#(
	.entrada(33),
	.left(0),
	.Nshifts(1) //Number of shifts
)
MULTI_SHIFT
(
	//Inputs
	.data(concatenated_data_wire),
	.right_left_flag(1),
	//Outputs
	.shifted_data(shifted_data_wire)
);
/************************************************************************************************
Output mux. 
This mux sends a 0 until the final data is ready and sends the product.
Entradas:
	[Selector]: 	Depending of the control is the value that is sending out.
	[MUX_Data0]: 	It's a zero represinting that the data is no finished yet.
	[MUX_Data1]:	It's the final data and is sent out when the algorithm is over.

Salidas:
	[MUX_Output]:  Data out dependind of the selector.
*/
Multiplexer2to1
#(
	.NBits(32)
)
MULTIPLICATION_MUX2TO1
(
	//inputs
	.Selector(ready_flag_wire),
	.MUX_Data0(0),
	.MUX_Data1({data_register_to_adder_wire,data_register_to_concatenation_wire[16:1]}),
	//Outputs
	.MUX_Output(data_final_output_wire)

);

multiplicationError
#(
	.DATA_LENGTH_32(32),
	.DATA_LENGTH_16(16)
)
ERROR_MULTIPLICATION
(
	.data(data_final_output_wire),
	.data_out(product_error_wire),
	.error(error_wire)
);
//Adder assigns
assign first_adder_output = data_register_to_adder_wire;
assign second_adder_output = data_to_adder_wire;

assign product = product_error_wire;
assign ready = ready_flag_wire;
assign error = error_wire;
////////////////////////////////////Compilation directive////////////////////////////////////////////
 /*Log Function*/
  function integer CeilLog2;
	 input integer data;
	 integer i,result;
	 begin
		 for(i=0; 2**i < data; i=i+1)
			 result = i + 1;
		 CeilLog2 = result;
	 end
 endfunction

endmodule