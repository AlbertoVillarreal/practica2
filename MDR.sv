module MDR
#(
	parameter Nbits = 16
)
(
		//Inputs
	input clk,
	input reset,
	input start,
	input load,
	input use_last_data,
	input [Nbits - 15 : 0]Opcode,
	input [Nbits - 1 : 0]Data_Input,
	
	//Outputs
	output ready,
	output error,
	output sign,
	output loadX_LED,
	output loadY_LED,
	output [Nbits - 1 : 0]result,
	output [Nbits - 1 : 0]remainder,
	output [Nbits - 10 : 0]units_display_result,
	output [Nbits - 10 : 0]tens_display_result,
	output [Nbits - 10 : 0]hundreds_display_result,
	output [Nbits - 10 : 0]thousands_units_display_result,
	output [Nbits - 10 : 0]thousands_tens_display_result
);

	bit SOR_OP_to_Adder_bit;
	bit MDRControl_loadX_to_DesSerial_loadX;
	bit MDRControl_loadY_to_DesSerial_loadY;
	bit MDRControl_startOp_to_OP_start;
	bit MDRControl_flush_to_ReadyRegisters;
	bit OP_readyFlag_to_MDRControl;
	bit div_sign_bit;
	bit operation_sign_bit;
	
	logic [Nbits - 1 : 0]Result_to_Deco7Seg;
	logic [Nbits - 1 : 0]Data1_Adder;
	logic [Nbits - 1 : 0]Data2_Adder;
	logic [Nbits - 1 : 0]Data_Feedback;
	logic [Nbits - 1 : 0]DesSerial_X_to_OP_X; 
	logic [Nbits - 1 : 0]DesSerial_Y_to_OP_Y;
	logic [Nbits - 1 : 0]OPResult_to_ReadyResult_Register;
	logic [Nbits - 1 : 0]OPRemainder_to_ReadyRemainder_Register;
	

/************************************************************************************************
	Tomamos los datos de entrada y los separamos en 2 registros distintos.
	Entradas:
		[loadX]:	Senial de control de Control_MDR para cargar el dato X, loadX
		[loadY]: Senial de control de Control_MDR para cargar el dato Y, loadY
		[Data_In]: Data_Input
		
	Salidas:
		[DataX_Out]: Dato X de modulo de ejecucion de operacion (layer 2)
		[DataY_Out]: Dato Y de modulo de ejecucion de operacion (layer 2)
	
*/	
Des_Serial
#(
	.Nbits(Nbits)
)
DES_SERIAL_MODULE
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.loadX(MDRControl_loadX_to_DesSerial_loadX),
	.loadY(MDRControl_loadY_to_DesSerial_loadY),
	.Data_In(Data_Input),
	
	//Outputs
	.DataX_Out(DesSerial_X_to_OP_X),
	.DataY_Out(DesSerial_Y_to_OP_Y)
);	
	
/************************************************************************************************

*/
Operation_Execution
#(
	.Nbits(Nbits)
)
LAYER_2
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(MDRControl_startOp_to_OP_start),
	.Opcode(Opcode),
	.DataFeedbackAdder(Data_Feedback),
	.DataX(DesSerial_X_to_OP_X),
	.DataY(DesSerial_Y_to_OP_Y),
	
	//Outputs
	.ready(OP_readyFlag_to_MDRControl),
	.error(error),
	.SOR(SOR_OP_to_Adder_bit),
	.div_sign(div_sign_bit),
	.data_adder_1(Data1_Adder),
	.data_adder_2(Data2_Adder),
	.result(OPResult_to_ReadyResult_Register),
	.remainder(OPRemainder_to_ReadyRemainder_Register)
	
);
	
/************************************************************************************************

*/
Adder
#(
	.variable_size(Nbits)
)
ADDER
(
	//Inputs
	.variable_sum1(Data1_Adder),
	.variable_sum2(Data2_Adder),
	.SOR(SOR_OP_to_Adder_bit),
	//Outputs
	.sum_output(Data_Feedback)
);

/************************************************************************************************
	Control general para modulo de MDR.
	
	Entradas:
		[start]: senial de inicio del sistema
		[load]:	senial para cargar los datos en el des-serializador
		[ready]:	senial de ejecucion de operacion
		[use_last_data]:	senial que indica que se hara otra operacion con los mismos datos de entrada
	
	Salidas:
		[loadX]: Senial de control para cargar dato X a des-serializador, MDRControl_loadX_to_DesSerial_loadX
		[loadXLED]: loadX_LED
		[loadY]: Senial de control para cargar dato Y a des-serializador, MDRControl_loadY_to_DesSerial_loadY
		[loadYLED]: loadY_LED
		[start_op]:	Iniciar operacion en modulo de ejecucion de operacion
		[flush]:	Registros de ready
	
*/
Control_MDR
MDR_CONTROL_MODULE
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(start),				
	.load(load),					
	.ready(OP_readyFlag_to_MDRControl),				
	.use_last_data(use_last_data),		
	
	//Outputs
	.loadX(MDRControl_loadX_to_DesSerial_loadX),				
	.loadXLED(loadX_LED),			
	.loadY(MDRControl_loadY_to_DesSerial_loadY),				
	.loadYLED(loadY_LED),			
	.start_op(MDRControl_startOp_to_OP_start),			
	.flush(MDRControl_flush_to_ReadyRegisters)					
	
);

/************************************************************************************************
	Registro de resultado. 
	Entradas:
		[enable]: senial de ready
		[sync_reset]: senial de flush de control
		[Data_Input]: resultado de ejecucion de operacion
	
	Salidas:
		[Data_Output]: result
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
READY_RESULT_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(OP_readyFlag_to_MDRControl),
	.sync_reset(MDRControl_flush_to_ReadyRegisters),
	.Data_Input(OPResult_to_ReadyResult_Register),

	// Output Ports
	.Data_Output(Result_to_Deco7Seg)
);

/************************************************************************************************
	Registro de residuo. 
	Entradas:
		[enable]: senial de ready
		[sync_reset]: senial de flush de control
		[Data_Input]: residuo de ejecucion de operacion
	
	Salidas:
		[Data_Output]: remainder
*/
Register_With_Clock_Enable
#(
	.Word_Length(Nbits)
)
READY_REMAINDER_REGISTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(OP_readyFlag_to_MDRControl),
	.sync_reset(MDRControl_flush_to_ReadyRegisters),
	.Data_Input(OPRemainder_to_ReadyRemainder_Register),

	// Output Ports
	.Data_Output(remainder)
);

/************************************************************************************************

*/
Decodificador
#(
	.word_length(Nbits)
)
DECODER_7_SEGMENTS
(
	//Input ports
	.data_input(Result_to_Deco7Seg),
	
	//Output ports
	.units_display_output(units_display_result),
	.tens_display_output(tens_display_result),
	.hundreds_display_output(hundreds_display_result),
	.thousands_units_display_output(thousands_units_display_result),
	.thousands_tens_display_output(thousands_tens_display_result),
	.sign(operation_sign_bit)
	
);


assign sign = (operation_sign_bit || div_sign_bit);
assign ready = OP_readyFlag_to_MDRControl;
assign result = Result_to_Deco7Seg;

endmodule