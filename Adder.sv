module Adder
#(
	parameter variable_size = 16
)
(
	//Inputs
	input signed[variable_size - 1 : 0]variable_sum1,
	input signed[variable_size - 1 : 0]variable_sum2,
	input SOR,
	//Outputs
	output signed[variable_size - 1 : 0]sum_output
);
	logic signed[variable_size - 1 : 0]sum_output_log/*synthesis keep*/;

always_comb
begin
	if(SOR)
		sum_output_log = variable_sum1 + variable_sum2;
	else
		sum_output_log = variable_sum1 - variable_sum2;
end

assign sum_output = sum_output_log;

endmodule