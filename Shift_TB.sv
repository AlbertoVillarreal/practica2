timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module Shift_TB;
	//Inputs
	logic [16 - 1 : 0]data_log;
	bit right_left_flag_bit = 1;
	//Outputs
	logic [16 - 1 : 0]shifted_data_log;
	
shift
DUT
(
	//Inputs
	.data(data_log),
	.right_left_flag(right_left_flag_bit),
	//Outputs
	.shifted_data(shifted_data_log)
	
);

/*initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  */
  /*********************************************************/
initial begin // reset generator
	#4 data_log = 31;
	#8 data_log = 15;
	
end

endmodule