module shift
#(
	parameter entrada = 16,
	parameter left = 0,
	parameter Nshifts = 2
)
(
	//Inputs
	input signed[entrada - 1 : 0]data,
	input right_left_flag,
	//Outputs
	output signed[entrada - 1 : 0]shifted_data
);
	logic signed[entrada - 1 : 0]shifted_data_log;

always_comb
begin
	if(right_left_flag == left)
		shifted_data_log = data <<< Nshifts;
	else
		shifted_data_log = data >>> Nshifts;

end
	assign shifted_data = shifted_data_log;
endmodule