module ControlIteration
#(
	parameter Nbits = 5
)
(
	//Inputs
	input [Nbits - 1 : 0]Counter,
	
	//Outputs
	output [Nbits : 0]Out
);

logic [Nbits : 0]Out_log;

always_comb
begin
	
	case(Counter)
		4'd0: Out_log = 5'd0;
		4'd1: Out_log = 5'd30;
		4'd2: Out_log = 5'd28;
		4'd3: Out_log = 5'd26;
		4'd4: Out_log = 5'd24;
		4'd5: Out_log = 5'd22;
		4'd6: Out_log = 5'd20;
		4'd7: Out_log = 5'd18;
		4'd8: Out_log = 5'd16;
		4'd9: Out_log = 5'd14;
		4'd10: Out_log = 5'd12;
		4'd11: Out_log = 5'd10;
		4'd12: Out_log = 5'd8;
		4'd13: Out_log = 5'd6;
		4'd14: Out_log = 5'd4;
		4'd15: Out_log = 5'd2;
		default: Out_log = 5'd0;
	endcase
end

assign Out = Out_log;

endmodule