module Multiplexor3to1
#(
	parameter NBits=16,
	parameter SELECTOR_BITS = 2
)
(
	//Inputs
	input [SELECTOR_BITS - 1 : 0]Selector,
	input [NBits-1:0] MUX_Data0,
	input [NBits-1:0] MUX_Data1,
	input [NBits-1:0] MUX_Data2,
	//Outputs
	output reg [NBits-1:0] MUX_Output

);
enum logic[1:0]{DATA0_OUT,DATA1_OUT,DATA2_OUT} cases;
logic [NBits-1:0]MUX_Output_wire;

	always_comb
	begin
		case(Selector)
			DATA0_OUT: MUX_Output_wire = MUX_Data0;
			
			DATA1_OUT: MUX_Output_wire = MUX_Data1;
	
			DATA2_OUT: MUX_Output_wire = MUX_Data2;
			
			default: MUX_Output_wire = MUX_Data0;
		
			endcase
	end
assign MUX_Output = MUX_Output_wire;
endmodule