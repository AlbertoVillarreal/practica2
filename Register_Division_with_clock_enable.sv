module Register_Division_with_clock_enable
#(
	parameter Word_Length = 16
)
(
	// Input Ports
	input clk,
	input reset,
	input [Word_Length-1:0] Data_Input,
	input final_condition_flag,
	input flush,
	// Output Ports
	output[Word_Length-1:0] Data_Output
);

logic [Word_Length-1:0] Data_Output_log/*synthesis keep*/;

always_ff@(posedge clk or negedge reset) begin:ThisIsARegister
	if(reset == 1'b0) 
		Data_Output_log <= {Word_Length{1'b0}};
	else 
		if(flush)
			Data_Output_log <= 0;
		else if(final_condition_flag == 0)
			Data_Output_log <= Data_Input;
end:ThisIsARegister

assign Data_Output = Data_Output_log;

endmodule
