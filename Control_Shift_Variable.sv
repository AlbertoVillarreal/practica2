module Control_Shift_Variable
#(
	parameter entrada = 16,
	parameter left = 1
)
(
	//Inputs
	input [entrada - 1 : 0]data,
	input [entrada - 12: 0]Nshifts,
	input right_left_flag,
	
	//Outputs
	output [entrada - 1 : 0]shifted_data
);
	logic [entrada - 1 : 0]shifted_data_log;

always_comb
begin
	if(right_left_flag == left)
		shifted_data_log = data << Nshifts;
	else
		shifted_data_log = data >> Nshifts;

end
	assign shifted_data = shifted_data_log;
endmodule