module Operation_Execution
#(
	parameter Nbits = 16
)
(
	//Inputs
	input clk,
	input reset,
	input start,
	input [Nbits - 15 : 0]Opcode,
	input [Nbits - 1 : 0]DataFeedbackAdder,
	input [Nbits - 1 : 0]DataX,
	input [Nbits - 1 : 0]DataY,
	
	//Outputs
	output ready,
	output error,
	output SOR,
	output div_sign,
	output [Nbits - 1 : 0]data_adder_1,
	output [Nbits - 1 : 0]data_adder_2,
	output [Nbits - 1 : 0]result,
	output [Nbits - 1 : 0]remainder
	
);

	bit start_counting_bit;
	bit finish_counting_bit;
	logic [Nbits - 13 : 0]counter_log;
	
	bit Multiplication_SOR_bit;
	bit Division_SOR_bit;
			
	bit DEMUX_StartFlag_to_MultiplicationStart;									//Demultiplexor de start a start de modulo de multiplicacion
	bit DEMUX_StartFlag_to_DivisionStart;											//Demultiplexor de start a start de modulo de division
	bit DEMUX_StartFlag_to_RootStart;												//Demultiplexor de start a start de modulo de raiz
	
	bit Ready_MultiplicationModule_to_MUX_Ready;									//Bandera de ready de modulo de multiplicacion a multiplexor de ready, MUX_Data0
	bit Ready_DivisionModule_to_MUX_Ready;											//Bandera de ready de modulo de division a multiplexor de ready, MUX_Data1
	bit Ready_RootModule_to_MUX_Ready;												//Bandera de ready de modulo de raiz a multiplexor de ready, MUX_Data2
	
	bit Error_MultiplicationModule_to_MUX_Error;									//Bandera de error de modulo de multiplicacion a multiplexor de error, MUX_Data0
	bit Error_DivisionModule_to_MUX_Error;											//Bandera de error de modulo de division a multiplexor de error, MUX_Data1
	bit Error_RootModule_to_MUX_Error;												//Bandera de error de modulo de raiz a multiplexor de error, MUX_Data2
	
	logic [Nbits - 1 : 0]Multiplication_Data1_to_MUX_Data1;					//Modulo de multiplicacion dato 1 a MUX de dato 1
	logic [Nbits - 1 : 0]Division_Data1_to_MUX_Data1;							//Modulo de division dato 1 a MUX de dato 1
	logic [Nbits - 1 : 0]Root_Data1_to_MUX_Data1;								//Modulo de raiz dato 1 a MUX de dato 1
	
	logic [Nbits - 1 : 0]Multiplication_Data2_to_MUX_Data2;					//Modulo de multiplicacion dato 2 a MUX de dato 2
	logic [Nbits - 1 : 0]Division_Data2_to_MUX_Data2;							//Modulo de division dato 2 a MUX de dato 2
	logic [Nbits - 1 : 0]Root_Data2_to_MUX_Data2;								//Modulo de raiz dato 2 a MUX de dato 2
	
	logic [Nbits - 1 : 0]Multiplication_Result_to_MUX_Result;				//Modulo de multiplicacion resultado a MUX de resultado
	logic [Nbits - 1 : 0]Division_Result_to_MUX_Result;						//Modulo de division resultado a MUX de resultado
	logic [Nbits - 1 : 0]Root_Result_to_MUX_Result;								//Modulo de raiz resultado a MUX de resultado
	
	logic [Nbits - 1 : 0]Division_Remainder_to_MUX_Remainder;				//Modulo de division residuo a MUX de residuo
	logic [Nbits - 1 : 0]Root_Remainder_to_MUX_Remainder;						//Modulo de raiz residuo a MUX de residuo
	
	logic [Nbits - 1 : 0]DEMUX_DataFeedback_to_MultiplicationFeedback;	//Demultiplexor de retroalimentacion 0 a dato de entrada de retroalimentacion de modulo de multiplicacion
	logic [Nbits - 1 : 0]DEMUX_DataFeedback_to_DivisionFeedback;			//Demultiplexor de retroalimentacion 1 a dato de entrada de retroalimentacion de modulo de division
	logic [Nbits - 1 : 0]DEMUX_DataFeedback_to_RootFeedback;					//Demultiplexor de retroalimentacion 2 a dato de entrada de retroalimentacion de modulo de raiz


/************************************************************************************************
	Demultiplexor 3 a 1 dato 1. 
	Se elige entre 3 salidas. Pasamos el dato de retroalimentacion del adder a un solo modulo del MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Input]: Dato de retroalimentacion del adder
	
	Salidas:

		[DEMUX_Data0]: Dato de retroalimentacion de modulo multiplicacion
		[DEMUX_Data1]: Dato de retroalimentacion de modulo division
		[DEMUX_Data2]: Dato de retroalimentacion de modulo raiz
*/	
Demultiplexer3to1
#(
	.NBits(Nbits),
	.SELECTOR_BITS(2) 
)
DEMUX_DATA_FEEDBACK
(
	//Inputs
	.Selector(Opcode),
	.DEMUX_Input(DataFeedbackAdder),
	
	//Outputs
	.DEMUX_Data0(DEMUX_DataFeedback_to_MultiplicationFeedback),
	.DEMUX_Data1(DEMUX_DataFeedback_to_DivisionFeedback),
	.DEMUX_Data2(DEMUX_DataFeedback_to_RootFeedback)
	
);
	
/************************************************************************************************
	Demultiplexor 3 a 1 dato 1. 
	Se elige entre 3 salidas. Pasamos la bandera de start a un solo modulo del MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[DEMUX_Input]: start.
	
	Salidas:

		[DEMUX_Data0]: Start de modulo multiplicacion
		[DEMUX_Data1]: Start de modulo division
		[DEMUX_Data2]: Start de modulo raiz
*/	
Demultiplexer3to1
#(
	.NBits(1),
	.SELECTOR_BITS(2) 
)
DEMUX_START_FLAG
(
	//Inputs
	.Selector(Opcode),
	.DEMUX_Input(start),
	
	//Outputs
	.DEMUX_Data0(DEMUX_StartFlag_to_MultiplicationStart),
	.DEMUX_Data1(DEMUX_StartFlag_to_DivisionStart),
	.DEMUX_Data2(DEMUX_StartFlag_to_RootStart)
	
);	

/************************************************************************************************
	Modulo de multiplicacion.
	Entradas:
		[multiplier]: DataX
		[multiplicand]: DataY 
		[adder]: Demultiplexor de retroalimentacion de adder,DEMUX_Data0 
		[start]: Demultiplexor de start, DEMUX_Data0
		
	Salidas:
		[product]:	
		[ready]:	Demultiplexor de senial de ready, MUX_Data0
		[first_adder_output]: 
		[second_adder_output]: 
*/
Multi_Booth
#(
	.DATALENGTH(Nbits),
	.DATALENGTH_PRODUCT((Nbits*2))
)
MULTIPLICATION_MODULE
(
	//Inputs
	.multiplier(DataX),
	.multiplicand(DataY),
	.adder(DEMUX_DataFeedback_to_MultiplicationFeedback),
	.start(DEMUX_StartFlag_to_MultiplicationStart),
	.clk(clk),
	.reset(reset),
	
	//Outputs
	.product(Multiplication_Result_to_MUX_Result),
	.ready(Ready_MultiplicationModule_to_MUX_Ready),
	.first_adder_output(Multiplication_Data1_to_MUX_Data1),
	.second_adder_output(Multiplication_Data2_to_MUX_Data2),
	.error(Error_MultiplicationModule_to_MUX_Error)

);

/************************************************************************************************
		Modulo de division.
		Entradas:
			[start]: Bandera para iniciar la operacion. Demultiplexor de start, DEMUX_Data1
			[dividend]: DataX
			[divisor]: DataY
			[adderFeedback]: Demultiplexor de retroalimentacion de adder,DEMUX_Data1
			
		Salidas:
			[ready]:
			[SOR]:
			[error]:
			[first_adder_output]:
			[second_adder_output]:
			[quotient]:
			[remainder]:
		
*/
Division_Module
#(
	.Nbits(Nbits)
)
DIVISION_MODULE
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(DEMUX_StartFlag_to_DivisionStart),
	.dividend(DataX),
	.divisor(DataY),
	.adderFeedback(DEMUX_DataFeedback_to_DivisionFeedback),
	
	//Outputs
	.ready(Ready_DivisionModule_to_MUX_Ready),
	.SOR(Division_SOR_bit),
	.error(Error_DivisionModule_to_MUX_Error),
	.sign(div_sign),
	.first_adder_output(Division_Data1_to_MUX_Data1),
	.second_adder_output(Division_Data2_to_MUX_Data2),
	.quotient(Division_Result_to_MUX_Result),
	.remainder(Division_Remainder_to_MUX_Remainder)
);

/************************************************************************************************
	Modulo de raiz.
	Entradas:
		[start]: Bandera para iniciar la operacion. Demultiplexor de start, DEMUX_Data2
		[finish_counting]: Bandera que indica fin de cuenta. Viene del contador.
		[counter]: Cuenta para controlar el corrimiento. Viene del contador.
		[data_in_Adder]: Dato de retroalimentacion del adder. Demultiplexor de feedback, DEMUX_Data2
		[data_in_X]: Dato de entrada X, DataX
		
	Salidas:
		[ready]:	Senial de ready.
		[start_counting]:	Senial para comenzar el conteo.
		[remainder]: Multiplexor de dato 1, MUX_Data2
		[result]: Multiplexor de dato 2, MUX_Data2
		[root_final_result]: Multiplexor de resultado, MUX_Data2
		[remainder_final_result]: Multiplexor de residuo, MUX_Data2
*/
Raiz
#(
	.Nbits(Nbits)
)
ROOT_MODULE
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start(DEMUX_StartFlag_to_RootStart),
	.finish_counting(finish_counting_bit),
	.counter(counter_log),
	.data_in_Adder(DataFeedbackAdder),
	.data_in_X(DataX),
	
	//Outputs
	.ready(Ready_RootModule_to_MUX_Ready),
	.start_counting(start_counting_bit),
	.error(Error_RootModule_to_MUX_Error),
	.remainder(Root_Data1_to_MUX_Data1),
	.result(Root_Data2_to_MUX_Data2),
	.root_final_result(Root_Result_to_MUX_Result),
	.remainder_final_result(Root_Remainder_to_MUX_Remainder)
);

CounterWithFunction
#(
	// Parameter Declarations
	.MAXIMUM_VALUE(Nbits),
	.NBITS_FOR_COUNTER(CeilLog2(Nbits))
)
COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(start_counting_bit),
	
	// Output Ports
	.flag(finish_counting_bit),
	.CountOut(counter_log) 
);
	
/************************************************************************************************
	Multiplexor 3 a 1 dato 1. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Dato 1 de modulo de multiplicacion
		[MUX_Data1]: Dato 1 de modulo de division
		[MUX_Data2]: Dato 1 de modulo de raiz
	
	Salidas:
		[MUX_Output]: data_adder_1. Salida directa al adder
*/
Multiplexor3to1
#(
	.NBits(Nbits),
	.SELECTOR_BITS(2)
)
MUX_DATA_1_ADDER
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(Multiplication_Data1_to_MUX_Data1),
	.MUX_Data1(Division_Data1_to_MUX_Data1),
	.MUX_Data2(Root_Data1_to_MUX_Data1),
	//Outputs
	.MUX_Output(data_adder_1)

);

/************************************************************************************************
	Multiplexor 3 a 1 dato 2. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Dato 2 de modulo de multiplicacion
		[MUX_Data1]: Dato 2 de modulo de division
		[MUX_Data2]: Dato 2 de modulo de raiz
	
	Salidas:
		[MUX_Output]: data_adder_2. Salida directa al adder
*/
Multiplexor3to1
#(
	.NBits(Nbits),
	.SELECTOR_BITS(2)
)
MUX_DATA_2_ADDER
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(Multiplication_Data2_to_MUX_Data2),
	.MUX_Data1(Division_Data2_to_MUX_Data2),
	.MUX_Data2(Root_Data2_to_MUX_Data2),
	//Outputs
	.MUX_Output(data_adder_2)

);

/************************************************************************************************
	Multiplexor 3 a 1 dato 1. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Bandera de ready de modulo de multiplicacion
		[MUX_Data1]: Bandera de ready de modulo de division
		[MUX_Data2]: Bandera de ready de modulo de raiz
	
	Salidas:
		[MUX_Output]: ready. 
*/
Multiplexor3to1
#(
	.NBits(1),
	.SELECTOR_BITS(2)
)
MUX_READY_FLAG
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(Ready_MultiplicationModule_to_MUX_Ready),
	.MUX_Data1(Ready_DivisionModule_to_MUX_Ready),
	.MUX_Data2(Ready_RootModule_to_MUX_Ready),
	//Outputs
	.MUX_Output(ready)

);

/************************************************************************************************
	Multiplexor 3 a 1 dato 1. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Bandera de error de modulo de multiplicacion
		[MUX_Data1]: Bandera de error de modulo de division
		[MUX_Data2]: Bandera de error de modulo de raiz
	
	Salidas:
		[MUX_Output]: error. 
*/
Multiplexor3to1
#(
	.NBits(1),
	.SELECTOR_BITS(2)
)
MUX_ERROR_FLAG
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(Error_MultiplicationModule_to_MUX_Error),
	.MUX_Data1(Error_DivisionModule_to_MUX_Error),
	.MUX_Data2(Error_RootModule_to_MUX_Error),
	//Outputs
	.MUX_Output(error)

);

/************************************************************************************************
	Multiplexor 3 a 1 dato 1. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Bandera de SOR de modulo de multiplicacion
		[MUX_Data1]: Bandera de SOR de modulo de division
		[MUX_Data2]: 0
	
	Salidas:
		[MUX_Output]: SOR. 
*/
Multiplexor3to1
#(
	.NBits(1),
	.SELECTOR_BITS(2)
)
MUX_SOR_FLAG
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(1),
	.MUX_Data1(Division_SOR_bit),
	.MUX_Data2(1),
	//Outputs
	.MUX_Output(SOR)

);

/************************************************************************************************
	Multiplexor 3 a 1 de resultado residuo. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: Resultado de multiplicacion
		[MUX_Data1]: Resultado de modulo de division
		[MUX_Data2]: Resultado de modulo de raiz
	
	Salidas:
		[MUX_Output]: result.
*/
Multiplexor3to1
#(
	.NBits(Nbits),
	.SELECTOR_BITS(2)
)
MUX_RESULT
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(Multiplication_Result_to_MUX_Result),
	.MUX_Data1(Division_Result_to_MUX_Result),
	.MUX_Data2(Root_Result_to_MUX_Result),
	//Outputs
	.MUX_Output(result)

);

/************************************************************************************************
	Multiplexor 3 a 1 de resultado residuo. 
	Se elige entre 3 entradas. Todas corresponden a un dato de salida de los modulos MDR.
	Como selector se utliliza el opcode ingresado por el usuario.
	Entradas:
		[Selector]: opcode.
		[MUX_Data0]: 0. En la multiplicacion no hay residuo
		[MUX_Data1]: Resultado de modulo de division
		[MUX_Data2]: Resultado de modulo de raiz
	
	Salidas:
		[MUX_Output]: remainder.
*/
Multiplexor3to1
#(
	.NBits(Nbits),
	.SELECTOR_BITS(2)
)
MUX_REMAINDER
(
	//Inputs
	.Selector(Opcode),
	.MUX_Data0(16'b0),
	.MUX_Data1(Division_Remainder_to_MUX_Remainder),
	.MUX_Data2(Root_Remainder_to_MUX_Remainder),
	//Outputs
	.MUX_Output(remainder)

);



//----------------------------------------------------------------------------------------------

/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

endmodule